DROP TABLE IF EXISTS `DETAILS`;
CREATE TABLE `DETAILS` (
  `d_ID` varchar(50) NOT NULL DEFAULT '',
  `d_Attribute` varchar(50) NOT NULL DEFAULT '',
  `d_Value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`d_ID`,`d_Attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
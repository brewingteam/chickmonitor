DROP TABLE IF EXISTS `TEMPS_LOG`;
CREATE TABLE `TEMPS_LOG` (
  `t_DateTime` datetime DEFAULT NULL,
  `t_Name` varchar(50) DEFAULT NULL,
  `t_Temp` decimal(7,2) DEFAULT NULL,
  `t_UOM` varchar(45) DEFAULT NULL,
  KEY `devices` (`t_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
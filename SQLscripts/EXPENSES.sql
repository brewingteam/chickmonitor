CREATE TABLE `ChickTemps`.`EXPENSES` (
  `e_ID` INT NULL AUTO_INCREMENT,
  `e_DateTime` DATETIME NULL,
  `e_Description` VARCHAR(45) NULL,
  `e_Ammount` DECIMAL(10,2) NULL,
  PRIMARY KEY (`e_ID`));

DROP TABLE IF EXISTS `UOM`;
CREATE TABLE `UOM` (
  `u_Name` varchar(50) NOT NULL DEFAULT '',
  `u_Description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`u_Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
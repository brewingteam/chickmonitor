CREATE TABLE `ChickTemps`.`CHICKENS` (
  `c_ID` INT NOT NULL AUTO_INCREMENT,
  `c_Name` VARCHAR(45) NULL,
  `c_Birthday` DATE NULL,
  `c_Breed` VARCHAR(45) NULL,
  `c_color` VARCHAR(45) NULL,
  PRIMARY KEY (`c_ID`));

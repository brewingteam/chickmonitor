LOCK TABLES TEMPS WRITE; -- the next prompt appears once you've obtained the lock
DROP TRIGGER Temp_History;
DELIMITER $$
CREATE DEFINER=`monitor`@`localhost` TRIGGER Temp_History AFTER UPDATE ON TEMPS FOR EACH ROW 
BEGIN
	INSERT INTO TEMPS_LOG(t_DateTime, t_Name, t_Temp, t_UOM) 
    select NEW.t_DateTime, D.d_Name, NEW.t_Temp, D.d_UOM FROM DEVICES D where D.d_Device = NEW.t_Device;
END
DELIMITER ;
UNLOCK TABLES;


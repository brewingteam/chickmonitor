use ChickTemps;

INSERT INTO DEVICES(d_Device, d_Name, d_Description, d_Active)
VALUES('/sys/bus/w1/devices/28-0417031d51ff','OutsideTemp','Chicken Coop Outside Temp',1);

INSERT INTO DEVICES(d_Device,d_Name,d_Description,d_Active)
VALUES('dht11.01h','CoopHumidity','Chicken Coop Inside Humidity',1);

INSERT INTO DEVICES(d_Device,d_Name,d_Description,d_Active)
VALUES('dht11.01t','CoopTemp','Chicken Coop Inside Temperature',1);

commit;


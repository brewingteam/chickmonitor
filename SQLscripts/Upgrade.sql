-- From V0 to V1
ALTER TABLE `ChickTemps`.`DEVICES` 
ADD COLUMN `d_UOM` VARCHAR(45) NULL DEFAULT NULL AFTER `d_Description`,
ADD COLUMN `d_Type` VARCHAR(45) NULL DEFAULT NULL AFTER `d_UOM`,
ADD COLUMN `d_Address` VARCHAR(45) NULL DEFAULT NULL AFTER `d_Type`;

ALTER TABLE `ChickTemps`.`TEMPS_LOG`
ADD COLUMN `t_UOM` varchar(45) DEFAULT NULL AFTER `t_Temp`;

DROP TRIGGER IF EXISTS `ChickTemps`.`Temp_History`;

DELIMITER $$
USE `ChickTemps`$$
CREATE DEFINER=`monitor`@`localhost` TRIGGER Temp_History AFTER UPDATE ON TEMPS FOR EACH ROW 
BEGIN
	INSERT INTO TEMPS_LOG(t_DateTime, t_Name, t_Temp, t_UOM) 
    select NEW.t_DateTime, D.d_Name, NEW.t_Temp, D.d_UOM FROM DEVICES D where D.d_Device = NEW.t_Device;
END$$
DELIMITER ;

UPDATE `ChickTemps`.`DETAILS` SET `d_Attribute`='Eggs' WHERE `d_ID`='Chicks' and`d_Attribute`='Egg Production';

INSERT INTO TABLE_VERSIONS (`v_Version`, `v_Table`) values (1, 'ANNOTATIONS');
INSERT INTO TABLE_VERSIONS (`v_Version`, `v_Table`) values (1, 'DETAILS');
INSERT INTO TABLE_VERSIONS (`v_Version`, `v_Table`) values (1, 'DEVICES');
INSERT INTO TABLE_VERSIONS (`v_Version`, `v_Table`) values (1, 'TEMPS');
INSERT INTO TABLE_VERSIONS (`v_Version`, `v_Table`) values (1, 'TEMPS_LOG');
INSERT INTO TABLE_VERSIONS (`v_Version`, `v_Table`) values (1, 'UOM');


-- From V1 to V2

CREATE TABLE `ChickTemps`.`ROLLS` (
  `r_Name` VARCHAR(45) NOT NULL,
  `r_Desc` VARCHAR(45) NULL,
  PRIMARY KEY (`r_Name`),
  UNIQUE INDEX `r_Name_UNIQUE` (`r_Name` ASC));

ALTER TABLE `ChickTemps`.`DEVICES` 
ADD CONSTRAINT `Roll`
  FOREIGN KEY (`d_Roll`)
  REFERENCES `ChickTemps`.`ROLLS` (`r_Name`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `ChickTemps`.`DEVICES` 
ADD COLUMN `d_Roll` VARCHAR(45) NULL DEFAULT NULL AFTER `d_UOM`,
ADD UNIQUE INDEX `d_Roll_UNIQUE` (`d_Roll` ASC);

INSERT INTO `ChickTemps`.`ROLLS` (`r_Name`, `r_Desc`) VALUES ('WaterTemp', 'device used to measure the temperature of the water container');
INSERT INTO `ChickTemps`.`ROLLS` (`r_Name`, `r_Desc`) VALUES ('WaterHeater', 'device used to turn the water heater on and off');
INSERT INTO `ChickTemps`.`ROLLS` (`r_Name`, `r_Desc`) VALUES ('CoopTemp', 'device used to measure the temperature inside the Coop');
INSERT INTO `ChickTemps`.`ROLLS` (`r_Name`, `r_Desc`) VALUES ('OutsideTemp', 'device used to measure the temperature outside the Coop');
INSERT INTO `ChickTemps`.`ROLLS` (`r_Name`, `r_Desc`) VALUES ('CoopHumidity', 'device used to measure the humidity inside the Coop');

UPDATE TABLE_VERSIONS SET `v_Version` = 2 WHERE `v_Table` = `ANNOTATIONS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 2 WHERE `v_Table` = `DETAILS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 2 WHERE `v_Table` = `DEVICES`;
UPDATE TABLE_VERSIONS SET `v_Version` = 2 WHERE `v_Table` = `ROLLS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 2 WHERE `v_Table` = `TEMPS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 2 WHERE `v_Table` = `TEMPS_LOG`;
UPDATE TABLE_VERSIONS SET `v_Version` = 2 WHERE `v_Table` = `UOM`;

-- the following section will need tweaking based on your device names
UPDATE `ChickTemps`.`DEVICES` SET `d_Roll`='WaterTemp' WHERE `d_Device`='/sys/bus/w1/devices/28-000005346d99';
UPDATE `ChickTemps`.`DEVICES` SET `d_Roll`='CoopTemp' WHERE `d_Device`='/sys/bus/w1/devices/28-00000534a0d9';
UPDATE `ChickTemps`.`DEVICES` SET `d_Roll`='OutsideTemp' WHERE `d_Device`='/sys/bus/w1/devices/28-00000534b6dd';
UPDATE `ChickTemps`.`DEVICES` SET `d_Roll`='CoopHumidity' WHERE `d_Device`='dht22.01h';

UPDATE `ChickTemps`.`DEVICES` SET `d_Type`='w1', `d_Address`='28-000005346d99' WHERE `d_Device`='/sys/bus/w1/devices/28-000005346d99';
UPDATE `ChickTemps`.`DEVICES` SET `d_Type`='w1', `d_Address`='28-00000534a0d9' WHERE `d_Device`='/sys/bus/w1/devices/28-00000534a0d9';
UPDATE `ChickTemps`.`DEVICES` SET `d_Type`='w1', `d_Address`='28-00000534b6dd' WHERE `d_Device`='/sys/bus/w1/devices/28-00000534b6dd';
UPDATE `ChickTemps`.`DEVICES` SET `d_Type`='w1', `d_Address`='28-00000534c954' WHERE `d_Device`='/sys/bus/w1/devices/28-00000534c954';
UPDATE `ChickTemps`.`DEVICES` SET `d_Type`='dht22', `d_Address`='27' WHERE `d_Device`='dht22.01h';
UPDATE `ChickTemps`.`DEVICES` SET `d_Type`='dht22', `d_Address`='27' WHERE `d_Device`='dht22.01t';

INSERT INTO `ChickTemps`.`DEVICES` (`d_Device`, `d_Name`, `d_Description`, `d_Roll`, `d_Type`, `d_Address`, `d_Active`) VALUES ('GPIO14', 'WaterHeater', 'WaterHeater relay', 'WaterHeater', 'GPIO', '14', '0');

-- From V2 to V3

CREATE TABLE `ChickTemps`.`USERS` (
  `u_UID` INT NULL AUTO_INCREMENT,
  `u_Name` VARCHAR(45) NOT NULL,
  `u_First` VARCHAR(45) NULL,
  `u_Last` VARCHAR(45) NULL,
  `u_Password` VARCHAR(45) NULL,
  PRIMARY KEY (`u_UID`, `u_Name`));
  
INSERT INTO `ChickTemps`.`USERS` (`u_Name`, `u_First`, `u_Last`, `u_Password`) VALUES ('Admin', 'Admin', 'User','$1$m0HqKFgC$Wfp2JMLXykABej76i7E1N1');

CREATE TABLE `ChickTemps`.`GROUPS` (
  `g_Name` VARCHAR(45) NOT NULL,
  `g_Description` VARCHAR(50) NULL,
  PRIMARY KEY (`g_Name`));
  
INSERT INTO `ChickTemps`.`GROUPS` (`g_Name`, `g_Description`) VALUES ('Admin', 'Site Admins');

CREATE TABLE `ChickTemps`.`USERS_GROUPS` (
  `ug_Name` VARCHAR(45) NOT NULL,
  `ug_Group` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ug_Group`, `ug_Name`));

INSERT INTO `ChickTemps`.`USERS_GROUPS` (`ug_Name`, `ug_Group`) VALUES ('Admin', 'Admin');


UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `ANNOTATIONS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `DETAILS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `DEVICES`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `ROLLS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `TEMPS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `TEMPS_LOG`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `UOM`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `USERS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `GROUPS`;
UPDATE TABLE_VERSIONS SET `v_Version` = 3 WHERE `v_Table` = `USERS_GROUPS`;

From V3 to V4

UPDATE `ChickTemps`.`DETAILS` SET `d_ID`='WaterHeater' WHERE `d_ID`='WaterTemp' and`d_Attribute`='MinTemp';
INSERT INTO `ChickTemps`.`DETAILS` (`d_ID`, `d_Attribute`, `d_Value`) VALUES ('WaterHeater', 'Enabled', '1');

From V4 to V5

ALTER TABLE `ChickTemps`.`ROLLS` 
RENAME TO  `ChickTemps`.`ROLES` ;

ALTER TABLE `ChickTemps`.`DEVICES` 
DROP FOREIGN KEY `Roll`;
ALTER TABLE `ChickTemps`.`DEVICES` 
CHANGE COLUMN `d_Roll` `d_Role` VARCHAR(45) NULL DEFAULT NULL ;
ALTER TABLE `ChickTemps`.`DEVICES` 
ADD CONSTRAINT `Roll`
  FOREIGN KEY (`d_Role`)
  REFERENCES `ChickTemps`.`ROLES` (`r_Name`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  ALTER TABLE `ChickTemps`.`DEVICES` 
DROP FOREIGN KEY `Roll`;
ALTER TABLE `ChickTemps`.`DEVICES` 
ADD CONSTRAINT `Role`
  FOREIGN KEY (`d_Role`)
  REFERENCES `ChickTemps`.`ROLES` (`r_Name`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  INSERT INTO `ChickTemps`.`DETAILS` (`d_ID`, `d_Attribute`, `d_Value`) VALUES ('WaterHeater', 'OnState', '0');
  
V5 to V6

ALTER TABLE `ChickTemps`.`TEMPS_LOG` 
CHANGE COLUMN `t_DateTime` `t_DateTime` DATETIME NOT NULL ,
CHANGE COLUMN `t_Name` `t_Name` VARCHAR(50) NOT NULL ,
ADD PRIMARY KEY (`t_DateTime`, `t_Name`);

ALTER TABLE `ChickTemps`.`USERS` 
CHANGE COLUMN `u_Password` `u_Password` VARCHAR(60) NULL DEFAULT NULL ;

V6 to V7
CREATE TABLE `ChickTemps`.`EXPENSES` (
  `e_ID` INT NULL AUTO_INCREMENT,
  `e_DateTime` DATETIME NULL,
  `e_Description` VARCHAR(45) NULL,
  `e_Ammount` DECIMAL(10,2) NULL,
  PRIMARY KEY (`e_ID`));

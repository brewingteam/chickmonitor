DROP TABLE IF EXISTS `ANNOTATIONS`;
CREATE TABLE `ANNOTATIONS` (
  `a_Device` varchar(50) NOT NULL DEFAULT '',
  `a_DateTime` datetime NOT NULL,
  `a_Title` varchar(50) DEFAULT NULL,
  `a_Description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`a_Device`,`a_DateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
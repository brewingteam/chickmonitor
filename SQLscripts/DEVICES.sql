DROP TABLE IF EXISTS `DEVICES`;
CREATE TABLE `DEVICES` (
  `d_Device` varchar(50) NOT NULL DEFAULT '',
  `d_Name` varchar(50) DEFAULT NULL,
  `d_Description` varchar(100) DEFAULT NULL,
  `d_UOM` varchar(45) DEFAULT NULL,
  `d_Roll` varchar(45) DEFAULT NULL,
  `d_Type` varchar(45) DEFAULT NULL,
  `d_Address` varchar(45) DEFAULT NULL,
  `d_Active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`d_Device`),
  UNIQUE KEY `d_Roll_UNIQUE` (`d_Roll`),
  CONSTRAINT `Roll` FOREIGN KEY (`d_Roll`) REFERENCES `ROLLS` (`r_Name`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
#Libraries
import sys
import os
import glob
import time
import datetime
import RPi.GPIO as GPIO
import MySQLdb

#import secure password file
import configparser
config = configparser.RawConfigParser()
config.read('/home/pi/secure.cfg')
dbUser = config.get('database', 'dbusername').strip('"')
dbPass = config.get('database', 'dbpassword').strip('"')
dbName = config.get('database', 'dbname').strip('"')

print("Current date and time: " , datetime.datetime.now())

db = MySQLdb.connect(host = "localhost", user = str(dbUser), passwd = str(dbPass), db = str(dbName))
curs=db.cursor()

#get min/max
LimitsQuery = "select D.d_Attribute, D.d_Value from ChickTemps.DETAILS D where D.d_ID = 'Feed';"
curs.execute (LimitsQuery)
records = curs.fetchall()
FeedLimitHigh=records[1][1]
FeedLimitLow=records[0][1]

print("Min/Max: "+str(FeedLimitHigh)+"/"+str(FeedLimitLow))

def write_db():
  try:
    #print "building string..."
    query = "INSERT INTO TEMPS SET t_Device = \'"
    query += "GPIO23"
    query += "\', t_Temp = "
    query += str(FeedLevel)
    query += " ON DUPLICATE KEY UPDATE "
    query += "t_Temp = "
    query += str(FeedLevel)
    query += ', t_DateTime = null'
    query += ";"
    print(query)
    
    curs.execute (query)
    db.commit()
    print("Data committed")
  except:
    print("Error: the database is being rolled back")
    db.rollback()

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#set GPIO Pins
GPIO_TRIGGER = 23
GPIO_ECHO = 24

samples = 10

#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

def read_temp_raw(who):
    f = open(who, 'r')
    lines = f.readlines()
    f.close()
    return lines

def getTempC():
  try:
    #print("getting CoopTemp from DB")
    query = "SELECT T.t_Temp, D.d_UOM FROM ChickTemps.TEMPS T LEFT JOIN DEVICES D ON T.t_Device = D.d_Device WHERE D.d_Role = 'CoopTemp';"        
    curs.execute(query)
    dbtemp = curs.fetchall()
  except:
    print("Error: Could not Find CoopTemp")

  if dbtemp[0][1] == "F":
    print("found temp in F: ",dbtemp[0][0])
    #Convert F to C (t°F − 32) × 5/9 = 0°C
    tempC = (dbtemp[0][0] -32) * 5/9
  elif dbtemp[0][1] == "C":
    print("found temp in C")
    tempC = dbtemp[0][0]
  else:
    print("could not convert coopTemp")
    print("using 20c")
    tempC = 20
  print("CoopTemp in C:",tempC)
        
  return tempC

def distance(temp):
  # set Trigger to HIGH
  GPIO.output(GPIO_TRIGGER, True)

  # set Trigger after 0.01ms to LOW
  time.sleep(0.00001)
  GPIO.output(GPIO_TRIGGER, False)

  StartTime = time.time()
  StopTime = time.time()

  # save StartTime
  while GPIO.input(GPIO_ECHO) == 0 and (time.time() - StopTime) < 5:
    StartTime = time.time()

  # save time of arrival
  while GPIO.input(GPIO_ECHO) == 1 and (time.time() - StartTime) < 5:
    StopTime = time.time()

  # time difference between start and arrival is round trip
  # half the time is one way
  TimeElapsed = (StopTime - StartTime) / 2
  #print("time: ",TimeElapsed)
  
  # The approximate speed of sound in dry (0% humidity) air, in metres per second, at temperatures near 0 °C, can be calculated from
  # C = (331.3 + 0.606 * t) m/s
  # where C = speed in m/s & t = temp in C
  speedOfSound = (331.3 + (0.606 * float(temp)))
  #print("Speed of sound: ",speedOfSound)

  # time * speed (m/s) = distance in m * 100 for cm
  distance = (TimeElapsed * speedOfSound) * 100
  #print("Distance: ",distance)

  return distance

try:
  tempC = getTempC()
  #print("its ", tempC)
  thislist = []
  for i in range(samples):
    dis = round(distance(tempC),1)
    print(dis)
    if dis <= int(FeedLimitHigh) and dis >= int(FeedLimitLow):
      thislist.append(dis)
    time.sleep(0.1)

  print(thislist)
  thislist.sort()
  FeedLevel = round((sum(thislist[1:samples-1]))/(len(thislist)-2))
  print("DIST: "+str(FeedLevel))
  if FeedLevel > int(FeedLimitLow) and FeedLevel < int(FeedLimitHigh):
    write_db()
  else:
    print("Reading was out of range!")
finally:
 GPIO.cleanup()
 sys.exit()

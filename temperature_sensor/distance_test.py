from gpiozero import DistanceSensor
#from gpiozero.pins.pigpio import PiGPIOFactory
from time import sleep

GPIO_TRIGGER = 23
GPIO_ECHO = 24
#factory = PiGPIOFactory()

print("setting up sensor")
sensor = DistanceSensor(echo=GPIO_ECHO, trigger=GPIO_TRIGGER, max_distance=5) #pin_factory=factory
i=0 
while i < 3:
  print("reading ", i)
  i=i+1
  print('Distance: ', sensor.distance)
  print(' value: ', sensor.value)
  sleep(3)

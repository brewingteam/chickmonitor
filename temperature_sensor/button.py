#!/usr/bin/env python2.7
import sys
import os
import glob
import time
import datetime
import MySQLdb
import RPi.GPIO as GPIO
import logging

logging.basicConfig(filename='/home/pi/chickmonitor/button.log',level=logging.INFO,format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

#import secure password file
import ConfigParser
config = ConfigParser.RawConfigParser()
config.read('/home/pi/secure.cfg')
dbUser = config.get('database', 'dbusername').strip('"')
dbPass = config.get('database', 'dbpassword').strip('"')
dbName = config.get('database', 'dbname').strip('"')

GPIO.setmode(GPIO.BCM)  
GPIO.setup(20, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP)

def Button_callback(channel):

    try:
        logging.info("Button push at: %s" , datetime.datetime.now())
        
        #connect to DB
        logging.debug("connecting to DB...")
        db=MySQLdb.connect(host = "localhost", user = str(dbUser), passwd = str(dbPass), db = str(dbName))
        curs=db.cursor()
        
        #check egg count for the day
        query = "SELECT count(*) count FROM ChickTemps.EGGS E where E.e_DateTime > DATE(now());"
        logging.debug(query)
        curs.execute (query)
        results = curs.fetchall()
        eggcount = results[0][0]
        logging.info("Eggs today: %s", eggcount)
        
        #if egg count < 5 add one
        if eggcount < 5:
            query = "INSERT INTO EGGS SET e_DateTime = now();"
            logging.debug(query)
            curs.execute (query)
            db.commit()
            logging.debug("Data committed")
            eggcount += 1
            blinkSpeed = 0.2
        else:
            logging.warning("Too many eggs today")
            blinkSpeed = .15

        #Flash light
        i=0
        for i in range(0,eggcount):
            GPIO.output(20,GPIO.HIGH)
            time.sleep(blinkSpeed)        
            GPIO.output(20, GPIO.LOW)
            time.sleep(blinkSpeed)


    except:
        logging.error("Error: the database is being rolled back")
        db.rollback()
    finally:
        logging.debug("closing DB connection")
        if db:
            db.close()
        #turn ligh off
        GPIO.output(20, GPIO.LOW)

GPIO.add_event_detect(21, GPIO.FALLING, callback=Button_callback, bouncetime=100)

try:
    logging.info("waiting for ghost button")
    GPIO.wait_for_edge(26, GPIO.FALLING)
   # while True:
   #    something = raw_input("Hit Enter")
   #     Button_callback(21)

except KeyboardInterrupt:  
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
GPIO.cleanup()           # clean up GPIO on normal exit

from gpiozero import CPUTemperature

# Use minimums and maximums that are closer to "normal" usage so the
# bar graph is a bit more "lively"
cpu = CPUTemperature(min_temp=50, max_temp=90)

#(0°C × 9/5) + 32
f = round(cpu.temperature * (9/5) + 32,2)
print('Initial temperature: {}F'.format(f))
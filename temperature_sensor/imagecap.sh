#!/bin/bash

echo "$(date)"

raspistill -rot 270 -v -o /var/www/html/images/test.jpg
convert -pointsize 40 -fill yellow -draw "text 270,160 '$(date)'" /var/www/html/images/test.jpg /var/www/html/images/test.jpg

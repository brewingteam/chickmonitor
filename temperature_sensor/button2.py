#!/usr/bin/python3
import time
import pymysql
import RPi.GPIO as GPIO
import logging
import threading
import configparser

#set up logging
logging.basicConfig(filename='/home/pi/chickmonitor/button.log',level=logging.DEBUG,format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

#get secure config values
config = configparser.RawConfigParser()
config.read('/home/pi/secure.cfg')
dbUser = config.get('database', 'dbusername').strip('"')
dbPass = config.get('database', 'dbpassword').strip('"')
dbName = config.get('database', 'dbname').strip('"')

#setup I/O
GPIO.setmode(GPIO.BCM)
GPIO.setup(20, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(21, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(26, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#Global Variables
ButtonPushCount = 0
ButtonPushTimout = 3 #seconds timout

def Button_callback(channel):
  global ButtonPushCount, ButtonPushTimer

  logging.info("Button push!")

  if ButtonPushCount < 5:
    ButtonPushTimer.cancel()
    ButtonPushCount +=1
    logging.debug("Adding %s Egg",ButtonPushCount)

    #flash once
    logging.debug("Flash")
    GPIO.output(20,GPIO.HIGH)
    time.sleep(.1)
    GPIO.output(20, GPIO.LOW)

    #Start Timer
    ButtonPushTimer = threading.Timer(ButtonPushTimout, AddEggs)
    ButtonPushTimer.start()
  else:
    logging.warning("Thats enough eggs for now.")
    #flash twice
    GPIO.output(20,GPIO.HIGH)
    time.sleep(.1)
    GPIO.output(20, GPIO.LOW)
    time.sleep(.1)
    GPIO.output(20, GPIO.HIGH)
    time.sleep(.1)
    GPIO.output(20, GPIO.LOW)


def AddEggs():
  global ButtonPushCount

  logging.info("adding %s eggs to DB.", ButtonPushCount)
  try:
    #connect to DB
    logging.debug("connecting to DB...")
    #db=MySQLdb.connect(host = "localhost", user = str(dbUser), passwd = str(dbPass), db = str(dbName))
    db=pymysql.connect(host = "localhost", user = str(dbUser), passwd = str(dbPass), db = str(dbName))
    curs=db.cursor()

    #check egg count for the day
    query = "SELECT count(*) count FROM ChickTemps.EGGS E where E.e_DateTime > DATE(now());"
    logging.debug(query)
    curs.execute (query)
    results = curs.fetchall()
    eggcount = results[0][0]
    logging.info("Eggs today: %s", eggcount)

    #if egg count < 5 add one
    for i in range(ButtonPushCount):
      if eggcount < 5:
        query = "INSERT INTO EGGS SET e_DateTime = now();"
        logging.debug(query)
        curs.execute (query)
        db.commit()
        logging.debug("Data committed")
        eggcount += 1
      else:
        logging.warning("Too many eggs today")

    #Flash light

    for i in range(0,eggcount):
      logging.debug("Flash")
      GPIO.output(20,GPIO.HIGH)
      time.sleep(.175)
      GPIO.output(20, GPIO.LOW)
      time.sleep(.175)

  except:
    logging.error("Error: the database is being rolled back")
    db.rollback()
  finally:
    logging.debug("closing DB connection")
    if db:
      db.close()
    ButtonPushCount = 0


## MAIN ##
logging.info("Starting Script")
ButtonPushTimer = threading.Timer(ButtonPushTimout, AddEggs)
GPIO.add_event_detect(21, GPIO.FALLING, callback=Button_callback, bouncetime=200)

something = ""
while True:
  #something != "Q":
  #something = input()
  #if something == "E":
  #  Button_callback("null")
  time.sleep(5)

GPIO.cleanup()
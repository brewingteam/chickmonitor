import os
import sys

def PIDLock(LockName):
    pid = os.getpid()
    pidfile = "/tmp/%s.pid" %(LockName)

    if os.path.isfile(pidfile):
        print "%s already exists." % pidfile
        try:
            os.kill(pid, 0)
        except OSError:
            print "no PID found, unlinking"
            os.unlink(pidfile)
        else:
            print "exiting"
            sys.exit()
    file(pidfile, 'w').write(str(pid))

def PIDUnLock(LockName):
    pidfile = "/tmp/%s.pid" %(LockName)
    os.unlink(pidfile)
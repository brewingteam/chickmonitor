#!/usr/bin/env python
import sys
import os
import glob
import time
import datetime
import MySQLdb
import Adafruit_DHT

#import secure password file
import ConfigParser
config = ConfigParser.RawConfigParser()
config.read('/home/pi/secure.cfg')
dbUser = config.get('database', 'dbusername').strip('"')
dbPass = config.get('database', 'dbpassword').strip('"')
dbName = config.get('database', 'dbname').strip('"')

print "Current date and time: " , datetime.datetime.now()

base_dir = '/sys/bus/w1/devices/'
device_folders = glob.glob(base_dir + '28*')

db = MySQLdb.connect(host = "localhost", user = str(dbUser), passwd = str(dbPass), db = str(dbName))
curs=db.cursor()

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def write_db():
    try:
        #print "building string..."
        query = "INSERT INTO TEMPS SET t_Device = \'"
        query += device
        query += "\', t_Temp = "
        query += str(temp_f)
        query += " ON DUPLICATE KEY UPDATE "
        query += "t_Temp = "
        query += str(temp_f)
        query += ', t_DateTime = null'
        query += ";"
        print query
        
        curs.execute (query)
        db.commit()
        print "Data committed"
    except:
        print "Error: the database is being rolled back"
        db.rollback()
        
#print device_folders
for device in device_folders:
    device_file = device + '/w1_slave'
    lines = read_temp_raw()
    #print "got lines"
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    #print "got temp - "
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = round(temp_c * 9.0 / 5.0 + 32.0,2)
        print "%s is  %s" %(device, temp_f)
        write_db()

#get DHT22 temps        
try:
    ### get any DHT22 devices
    query = "Select d_Device, d_UOM, d_Type, d_Address from DEVICES where (d_Type = 'dht22' OR d_Type = 'dht11') and d_Active = 1;"        
    curs.execute(query)
    curs.fetchall()
except:
    print "Error: Could not Find any dht22 devices. Disabled by default"

for device in curs:
    if device[2].upper() == 'DHT11':
        devicetype = Adafruit_DHT.DHT11
    elif device[2].upper() == 'DHT22':
        devicetype = Adafruit_DHT.DHT22
        
    
    print "trying to read "+ device[2].upper() +" device on pin: "+ str(device[3])
    (humidity, temperature) = Adafruit_DHT.read_retry(devicetype, device[3])

    if humidity is not None and temperature is not None:
        temperature = round(temperature * 9 / 5 + 32,2)
        humidity = round(humidity,2)

        # print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))

        try:
            if device[1] == '%' and humidity < 100 and humidity > 0:
                query = "INSERT INTO TEMPS SET t_Device = \'"+str(device[0])+"\'" 
                query += ", t_Temp = "
                query += str(humidity)
                query += ' ON DUPLICATE KEY UPDATE '
                query += 't_Temp = '
                query += str(humidity)
                query += ', t_DateTime = null'
                query += ';'
                print query
                curs.execute(query)
                db.commit()
            elif device[1] == 'F'and temperature < 200 and temperature > -100:
                query = "INSERT INTO TEMPS SET t_Device = \'"+str(device[0])+"\'" 
                query += ", t_Temp = "
                query += str(temperature)
                query += ' ON DUPLICATE KEY UPDATE '
                query += 't_Temp = '
                query += str(temperature)
                query += ', t_DateTime = null'
                query += ';'
                print query
                curs.execute(query)
                db.commit()
                print 'DHT22 Data committed'
        except:
            print 'Error in DHT22: the database is being rolled back'
            db.rollback()
sys.exit()

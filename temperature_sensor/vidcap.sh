#!/bin/bash
sleep 10s
echo "$(date)"
# Capture 30 seconds of raw video at 640x480 and 150kB/s bit rate into a pivideo.h264 file:
raspivid -t 10000 -w 640 -h 480 -fps 25 -b 1200000 -rot 270 -p 0,0,640,480 -o /var/www/html/pivideo.h264 

# Wrap the raw video with an MP4 container: 
rm /var/www/html/video.mp4 -f
MP4Box -add /var/www/html/pivideo.h264 /var/www/html/video.mp4

# Remove the source raw file, leaving the remaining pivideo.mp4 file to play
rm /var/www/html/pivideo.h264 -f

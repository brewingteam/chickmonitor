import sys
import RPi.GPIO as GPIO
import time
import datetime
import MySQLdb
import PIDfile

#print time /date this is useful for CRON job log.
print "Current date and time: " , datetime.datetime.now()

#check to see if this is already running, DIE if so
PIDfile.PIDLock("Water_Heater")

#import secure password file
import ConfigParser
config = ConfigParser.RawConfigParser()
config.read('/home/pi/secure.cfg')
dbUser = config.get('database', 'dbusername').strip('"')
dbPass = config.get('database', 'dbpassword').strip('"')
dbName = config.get('database', 'dbname').strip('"')

db = MySQLdb.connect(host = "localhost", user = str(dbUser), passwd = str(dbPass), db = str(dbName))
db.autocommit(True);
curs=db.cursor()


def IsHeaterEnabled():
    try:
        ### Set temp sensor address
        query = "Select * from DETAILS where d_ID = 'WaterHeater' and d_Attribute = 'Enabled';"        
        curs.execute (query)
        curs.fetchall()
        for row in curs:
            print "Water heater Enabled is %s" %(str(row[2]))
            HeaterEnabled = row[2]
    except:
        print "Error: Could not Find WaterHeater Enabled bit. Disabled by default"
        HeaterEnabled = 0
    return HeaterEnabled

def SetMinTemp():
    try:
        ### Set MinTemp
        query = "Select * from DETAILS where d_ID = 'WaterHeater' AND d_Attribute = 'MinTemp';"        
        curs.execute (query)
        curs.fetchall()
        for row in curs:
            print "setting MinTemp to %s" %(row[2]) 
            MinTemp = int(row[2])
    except:
        print "Error: Could not Find MinTemp Hard coding to 35"
        MinTemp = 35
    return MinTemp

def SetHeatTo():    
    try:
        ### Set HeatTo
        query = "Select * from DETAILS where d_ID = 'WaterHeater' AND d_Attribute = 'HeatTo';"        
        curs.execute (query)
        curs.fetchall()
        for row in curs:
            print "setting HeatTo (swing) to %s" %(row[2]) 
            HeatTo = int(row[2])
    except:
        print "Error: Could not Find HeatTo Hard coding to 45"
        HeatTo = 45
    return HeatTo
    
    
try:
   ### Set Heater Pin Number
   query = "Select * from DEVICES where d_Role = 'WaterHeater';"        
   curs.execute (query)
   curs.fetchall()
   for row in curs:
       #print(row)
       if row is not None:
          print "setting pin %s" %(str(row[6]))
          HeaterRelayPin = int(row[6])
          HeaterID = int(row[0])
       else:
          print "Error finding WaterHeater"
          HeaterRelayPin = 14
          HeaterID = 'GPIO14'
except:
   print "Error: Could not Find waterHeater hardcoding to 14"
   HeaterRelayPin = 14
   HeaterID = 'GPIO14'

try:
    ### Set temp sensor address
    query = "Select * from DEVICES where d_Role = 'WaterTemp';"        
    curs.execute (query)
    curs.fetchall()
    for row in curs:
        print "setting heater probe to %s" %(str(row[6]))
        HeaterTempProbeID = '/sys/bus/w1/devices/' + row[6] + '/w1_slave'
except:
    print "Error: Could not Find waterTemp Hard coding to 28-000005346d99"
    HeaterTempProbeID = '/sys/bus/w1/devices/' + '28-000005346d99' + '/w1_slave'

try:
    ### Set Heater ON state (NO OR NC relay)
    query = "Select d_Value from DETAILS where d_ID = 'WaterHeater' AND d_Attribute ='OnState';"        
    curs.execute (query)
    curs.fetchall()
    for row in curs:
        HeaterOnState = (bool(int(row[0])))
        print "setting HeaterOnState to %s" %(HeaterOnState)
except:
    print "Error: Could not Find waterTemp Hard coding to 28-000005346d99"
    HeaterTempProbeID = '/sys/bus/w1/devices/' + '28-000005346d99' + '/w1_slave'


#also need to set NO or NC state here. the relay I'm using uses a normaly open on HIGH signal.
#GPIO.setmode(GPIO.BOARD)
GPIO.setmode(GPIO.BCM)
GPIO.setup(HeaterRelayPin, GPIO.OUT, initial=GPIO.HIGH)

#duty cycle
LoopTime = 30
MaxRunTime = 60
# MinTemp = 35
Mode = 'noRest' #Rest/noRest

#id like to push this off to it's own script to query actual hardware

def read_temp_raw():
    #print 'reading %s' %(HeaterTempProbeID)
    f = open(HeaterTempProbeID, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    #print "got lines"
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = round(temp_c * 9.0 / 5.0 + 32.0,2)
    #print "water is %s" %(temp_f)
    return temp_f

try:
    if (IsHeaterEnabled() == '0'):
    # Heater is Disabled, no need to go any further. Shut this mother down.
        print "Water Heater is disabled."
        sys.exit(0)
    
    MinTemp = SetMinTemp()
    HeatTo = SetHeatTo()
    cur_temp = read_temp()
    if (cur_temp < MinTemp):
        #turing heater on. we need to insert 2 records to avoid a saw tooth chart.
        query = "insert into TEMPS set t_DateTime = NOW() - INTERVAL 1 MINUTE, t_Device = '" +HeaterID +"', t_Temp = 0 ON DUPLICATE KEY UPDATE t_Temp=0, t_DateTime= NOW() - INTERVAL 1 MINUTE;"
        curs.execute (query)
        query = "insert into TEMPS set t_Device = '" +HeaterID +"', t_Temp = 1 ON DUPLICATE KEY UPDATE t_Temp=1, t_DateTime=NULL;"
        curs.execute (query)
        db.commit()
        while (cur_temp < HeatTo) and (IsHeaterEnabled() == '1'):
            MinTemp = SetMinTemp()
            HeatTo = SetHeatTo()
            GPIO.output(HeaterRelayPin, HeaterOnState)
            cur_temp = read_temp()
            print "Heating: %s" %(cur_temp)
            time.sleep(5)
        
    print "water is %s" %(cur_temp)    
    print "make sure heater is off"
    if (bool(GPIO.input(HeaterRelayPin)) == bool(HeaterOnState)):
        GPIO.output(HeaterRelayPin, not HeaterOnState)
        query = "insert into TEMPS set t_DateTime = NOW() - INTERVAL 1 MINUTE, t_Device = '" +HeaterID +"', t_Temp = 1 ON DUPLICATE KEY UPDATE t_Temp=1, t_DateTime= NOW() - INTERVAL 1 MINUTE;"
        curs.execute (query)
        query = "insert into TEMPS set t_Device = '" +HeaterID +"', t_Temp = 0 ON DUPLICATE KEY UPDATE t_Temp=0, t_DateTime=NULL;"
        curs.execute (query)
        db.commit()
        print "heater is off"
finally:  
    GPIO.cleanup() # this ensures a clean exit
    curs.close()
    del curs
    PIDfile.PIDUnLock("Water_Heater")

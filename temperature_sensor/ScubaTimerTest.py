#!/usr/bin/python3
import time
import pymysql
import logging
import threading



#FUNCTIONS
class ScubaTimer:
  def __init__(self,ttime,tfunction):
    self.is_started = False
    self.ttime = ttime
    self.tfunction= tfunction
  def start(self):
    self.is_started = True
    self.started_at = time.time()
    self.timer = threading.Timer(self.ttime,self.finished)
    self.timer.start()
  def elapsed(self):
    if self.is_started:
      return time.time() - self.started_at
    else:
      return 0
  def remaining(self):
    if self.is_started:
      return self.interval - self.elapsed()
    else:
      return 0
  def restart(self):
    if self.is_started:
      self.timer.cancel()
      self.timer = threading.Timer(self.ttime,self.finished)
      self.timer.start()
  def finished(self):
    self.is_started = False
    self.tfunction()

def Finished():
  print('Timer Expired')

#MAIN
ButtonPushTimout = 5


ButtonPushTimer = ScubaTimer(ButtonPushTimout, Finished)
while True:
  userIn = input("push enter to start")
  
  if (ButtonPushTimer.is_started):
    if (ButtonPushTimer.elapsed() < 1):
      print('Just Started')
      print(ButtonPushTimer.elapsed())
    else:
      print('restarting timer')
      ButtonPushTimer.restart()
      print(ButtonPushTimer.elapsed())
  else:
    print('starting timer')
    ButtonPushTimer.start()



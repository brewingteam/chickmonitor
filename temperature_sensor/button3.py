#!/usr/bin/python3
import time
import pymysql
import logging
import threading
import configparser
from gpiozero import LED, Button
from signal import pause
from subprocess import check_call

#set up logging
logging.basicConfig(filename='/home/pi/chickmonitor/button.log',level=logging.DEBUG,format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

#get secure config values
config = configparser.RawConfigParser()
config.read('/home/pi/secure.cfg')
dbUser = config.get('database', 'dbusername').strip('"')
dbPass = config.get('database', 'dbpassword').strip('"')
dbName = config.get('database', 'dbname').strip('"')

#I/O Setup
light = LED(20)
button = Button(21,hold_time=5)

#Global Variables
ButtonPushCount = 0
ButtonPushTimout = 3 #seconds timout
ShuttingDown = False

class ScubaTimer:
  def __init__(self,ttime,tfunction):
    self.is_started = False
    self.ttime = ttime
    self.tfunction= tfunction
  def start(self):
    if self.is_started:
      self.timer.cancel()
    self.is_started = True
    self.started_at = time.time()
    self.timer = threading.Timer(self.ttime,self.finished)
    self.timer.start()
  def elapsed(self):
    if self.is_started:
      return time.time() - self.started_at
    else:
      return 0
  def remaining(self):
    if self.is_started:
      return self.interval - self.elapsed()
    else:
      return 0
  def finished(self):
    self.is_started = False
    self.tfunction()

def Button_push():
  global ButtonPushCount, ButtonPushTimer, ShuttingDown

  if (not ShuttingDown):
    if ((ButtonPushTimer.is_started) and (ButtonPushTimer.elapsed() > 0.75)) or (not ButtonPushTimer.is_started):
      logging.info("Button push!")
      if ButtonPushCount < 5:
        ButtonPushTimer.start()
        ButtonPushCount +=1
        logging.debug("Adding %s Egg",ButtonPushCount)
        #flash once
        light.blink(0.5,0.5,1)
      else:
        logging.warning("Thats enough eggs for now.")
        #flash twice
        light.blink(0.1,0.1,5,False)
    else:
      logging.debug("DEBOUNCE!")

def AddEggs():
  global ButtonPushCount

  logging.info("adding %s eggs to DB.", ButtonPushCount)
  try:
    #connect to DB
    logging.debug("connecting to DB...")
    #db=MySQLdb.connect(host = "localhost", user = str(dbUser), passwd = str(dbPass), db = str(dbName))
    db=pymysql.connect(host = "localhost", user = str(dbUser), passwd = str(dbPass), db = str(dbName))
    curs=db.cursor()

    #check egg count for the day
    query = "SELECT count(*) count FROM ChickTemps.EGGS E where E.e_DateTime > DATE(now());"
    logging.debug(query)
    curs.execute (query)
    results = curs.fetchall()
    eggcount = results[0][0]
    logging.info("Eggs today: %s", eggcount)

    #if egg count < 5 add one
    for i in range(ButtonPushCount):
      if eggcount < 5:
        query = "INSERT INTO EGGS SET e_DateTime = now();"
        logging.debug(query)
        curs.execute (query)
        db.commit()
        logging.debug("Data committed")
        eggcount += 1
      else:
        logging.warning("Too many eggs today")

    #Flash light
    logging.debug("Flash")
    light.blink(0.25,0.25,eggcount,False)

  except:
    logging.error("Error: the database is being rolled back")
    db.rollback()
  finally:
    logging.debug("closing DB connection")
    if db:
      db.close()
    ButtonPushCount = 0

def reboot():
  global ShuttingDown
  ShuttingDown = True
  light.blink(0.1,0.1,10)
  check_call(['sudo', 'reboot'])

## MAIN ##
logging.info("Starting Script")

ButtonPushTimer = ScubaTimer(ButtonPushTimout, AddEggs)
button.when_released = Button_push
button.when_held = reboot

pause()

import time
import threading

CurrentNumber = 0
NewCount = 0


def additem():
  global NewCount
  global WorkTimer

  WorkTimer.cancel()
  print("adding 1 to ",NewCount)
  NewCount +=1
  WorkTimer = threading.Timer(5,DoWork)
  WorkTimer.start()


def subitem():
  global NewCount
  global WorkTimer

  WorkTimer.cancel()
  print("subtracting 1 from ",NewCount)
  NewCount -=1
  WorkTimer = threading.Timer(5,DoWork)
  WorkTimer.start()

def DoWork():
  global CurrentNumber, NewCount

  CurrentNumber += NewCount
  print("\nCurrentNumber = ", CurrentNumber)
  NewCount = 0

WorkTimer = threading.Timer(5,DoWork)
while True:
  userin = input(">")
  if userin == "i":
    additem()
  elif userin == "d":
    subitem()


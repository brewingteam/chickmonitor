<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . "/Header.php";

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$numdays = 30;
if( isset($_POST['date']) )
{
  $numdays = $_POST["date"];
}


?> 
    <!--Load the AJAX API-->
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      //google.charts.load('current', {'packages':['corechart']});
      //google.charts.load('visualization', '1', {packages: ['controls']});
      google.charts.load('current', {'packages':['line', 'corechart']});
      
      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      
      function drawChart() {
        var FeedLevelData = $.ajax({
          type: "POST",
          url: "FeedData.php",
          data:{date:"<?php echo $numdays ?>",device:"FeedLevel"},
          dataType: "json",
          async: false
        }).responseText;
        
        // Create our data table out of JSON data loaded from server.
        var FeedLevelDataTable = new google.visualization.DataTable(FeedLevelData);

        
        var FeedLevelOptions = {
          title: 'Feed Levels',
          curveType: 'None',
          legend: { position: 'bottom' },
          interpolateNulls: true,
          series: {
            // Gives each series an axis name that matches the Y-axis below.
            0: {targetAxisIndex: 1, color: 'black'}
          },
          vAxes: {
            // Adds labels to each axis; they don't have to match the axis names.
            0: {title: 'FeedLevel (%)',
                //format: 'percent',
                maxValue: 100,
                minValue: 0,
              }
          },
          trendlines: {
            0: {
              type: 'polynomial', //'exponential',
              degree: 20,
              pointsVisible: false,
              labelInLegend: 'Average Feed Level',
              visibleInLegend: true,
              color: 'green',
            }
          }
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(FeedLevelDataTable, FeedLevelOptions);
      }

    </script>
    

  </head>
  <body class="normal">
    <?php include_once "Nav.html"; ?>
    <div class="w3-row-padding">
      <form action="/FeedStats.php" method="post">
        Days (max 180):
        <input type="number" name="date" min="0" max="180" value="<?php echo $numdays ?>">
          <input type="submit">
      </form>
    </div>
    <div id="dashboard">
      <div id="chart_div" style="width: 95vw; height: 90vh; margin: 0 auto; "></div>
      <div id="chart2_div" style="width: 95vw; height: 90vh; margin: 0 auto; "></div>
      <div id="range_filter_div"></div>
    </div>
  </body>
</html>
<?php
  include_once $path . "/Footer.php";
?>

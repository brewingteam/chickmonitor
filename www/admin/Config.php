<?php
// Start the session
session_start();

if (!isset($_SESSION["User"])) {
	header('Location: ./Index.php');
}

$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . "/Header.php";

if ((isset($_POST["mode"])) AND ($_POST["mode"] == "submit")){
    print_r("Updating Settings....");
    
    dbQuery("update DETAILS set d_Value = '".$_POST['city']."' where d_ID = 'Location' and d_Attribute = 'City'");
    dbQuery("update DETAILS set d_Value = '".$_POST['SiteName']."' where d_ID = 'Site' and d_Attribute = 'Name'");
    dbQuery("update DETAILS set d_Value = '".$_POST['DOB']."' where d_ID = 'Chicks' and d_Attribute = 'DOB'");
    dbQuery("update DETAILS set d_Value = '".$_POST['Eggs']."' where d_ID = 'Chicks' and d_Attribute = 'Eggs'");
    
    dbQuery("update DETAILS set d_Value = '".$_POST['MinTemp']."' where d_ID = 'WaterHeater' and d_Attribute = 'MinTemp'");
    dbQuery("update DETAILS set d_Value = '".$_POST['HeatTo']."' where d_ID = 'WaterHeater' and d_Attribute = 'HeatTo'");
    if ((isset($_POST['HeaterEnabled'])) AND ($_POST['HeaterEnabled'] == "on")){$WaterHeater = 1;}else{$WaterHeater=0;}
    dbQuery("update DETAILS set d_Value = '".$WaterHeater."' where d_ID = 'WaterHeater' and d_Attribute = 'Enabled'");
    
    dbQuery("update DETAILS set d_Value = '".$_POST['WuderApi']."' where d_ID = 'Wunderground' and d_Attribute = 'APIkey'");
    dbQuery("update DETAILS set d_Value = '".$_POST['AccuApi']."' where d_ID = 'Accuweather' and d_Attribute = 'APIkey'");
    dbQuery("update DETAILS set d_Value = '".$_POST['DarkSkyApi']."' where d_ID = 'DarkSky' and d_Attribute = 'APIkey'");
    
    if (isset($_POST["SetUserPass"]))
    {
        if (($_POST["SetUserPass"] == $_POST["SetUserPassConfirm"]) and (!empty($_POST["SetUserPass"]) ))
        {
            $passhash = password_hash($_POST["SetUserPass"],PASSWORD_DEFAULT);
            print_r($passhash);
            dbQuery("update USERS set u_Password = '".$passhash."' where u_Name = '".$_SESSION["User"]."'");
        }
    }
    
    $_POST["mode"] = "refresh";
    header("Refresh:0");
}

$HeaterState = dbQuery("Select * from DETAILS where d_ID = 'WaterHeater' and d_Attribute = 'Enabled'");
$WaterTemp = dbQuery("Select * from DETAILS where d_ID = 'WaterHeater' and d_Attribute = 'MinTemp'");
$HeatTo = dbQuery("Select * from DETAILS where d_ID = 'WaterHeater' and d_Attribute = 'HeatTo'");
$OnState = dbQuery("Select * from DETAILS where d_ID = 'WaterHeater' and d_Attribute = 'OnState'");

$Location = dbQuery("SELECT * from DETAILS where d_ID = 'Location' and d_Attribute = 'City'");
$SiteName = dbQuery("SELECT * from DETAILS where d_ID = 'Site' and d_Attribute = 'Name'");
$DOB = dbQuery("SELECT * from DETAILS where d_ID = 'Chicks' and d_Attribute = 'DOB'");
$Eggs = dbQuery("SELECT * from DETAILS where d_ID = 'Chicks' and d_Attribute = 'Eggs'");

$WunderG = dbQuery("SELECT * from DETAILS where d_ID = 'Wunderground' and d_Attribute = 'APIkey'");
$AccuW = dbQuery("SELECT * from DETAILS where d_ID = 'Accuweather' and d_Attribute = 'APIkey'");
$DarkSky = dbQuery("SELECT * from DETAILS where d_ID = 'DarkSky' and d_Attribute = 'APIkey'");

$Devices = dbQuery("SELECT * from DEVICES");

$group = dbQuery("SELECT G.g_Name from GROUPS G RIGHT JOIN USERS U ON U.u_Name = 'Admin'");
?>
<script>
    <!--include javascript here-->
</script>
</head>
	<body class="normal">
		<?php include_once $path ."/Nav.html"; ?>
		<h1>Configuration</h1>
		<form action="Config.php" method="POST">
        <input type="hidden" name="mode" value="submit"/>
         <fieldset>
            <legend>Water Heater</legend>
            <?php if($HeaterState[0]['d_Value'] == 1){
                    //echo "Heater is on";
                    echo "Heater enabled:<label class=\"switch\">  <input type=\"checkbox\" checked name=\"HeaterEnabled\">  <span class=\"slider\"></span></label><br><br>";
                }else{
                    //echo "Heater is off";
                    echo "Heater enabled:<label class=\"switch\">  <input type=\"checkbox\" name=\"HeaterEnabled\">  <span class=\"slider\"></span></label><br><br>";
                }
            ?>
            Min Water Temp: <input type="number" name="MinTemp" min="0" max="90" value=<?PHP echo $WaterTemp[0]['d_Value'] ?>><br/>
            Heat To (swing) Temp: <input type="number" name="HeatTo" min="0" max="90" value=<?PHP echo $HeatTo[0]['d_Value'] ?>><br/>
            The heater will kick on at &ltMinTemp&gt and heat until it reaches &ltHeatTo&gt. aka Swing on a thermostat.<br/>
            Heater OnState : <input type="number" name="OnState" min="0" max="1" value=<?PHP echo $OnState[0]['d_Value'] ?>><br/>
            OnState is the condition for the relay to be ON (heating). for a NO relay is should be 1(true) for a NC relay it should be 0 (false)
        </fieldset>
        <fieldset>
            <legend>Site Info</legend>
            Site Name: <input type="text" name="SiteName" value="<?php print_r($SiteName[0]['d_Value'])?>"/><br/>
            Location (city): <input type="text" name="city" value="<?php print_r($Location[0]['d_Value'])?>"/><br/>
            Chicks DOB: <input type="date" name="DOB" value="<?php print_r($DOB[0]['d_Value'])?>"/><br/>
            Chicks First Eggs: <input type="date" name="Eggs" value="<?php print_r($Eggs[0]['d_Value'])?>"/><br/>
        </fieldset>
        <fieldset>
        <legend>Forecast APIs</legend>
        Wunderground: <input type="text" name="WuderApi" value="<?php print_r($WunderG[0]['d_Value'])?>"/><br/>
        Accuweather: <input type="text" name="AccuApi" value="<?php print_r($AccuW[0]['d_Value'])?>"/>
        Accuweather: <input type="text" name="DarkSkyApi" value="<?php print_r($DarkSky[0]['d_Value'])?>"/>
        </fieldset>
        <fieldset>
        <legend>Devices</legend>
          <div class="datatable">
            <?PHP
                #print_r($Devices);
                for ($x = 0; $x < count($Devices); $x++) {
                    print_r("<input type=\"text\" value=\"".$Devices[$x]['d_Device']."\">");
                    print_r("<input type=\"text\" value=\"".$Devices[$x]['d_Name']."\">");
                    print_r("<input type=\"text\" value=\"".$Devices[$x]['d_Description']."\">");
                    print_r("<input type=\"text\" value=\"".$Devices[$x]['d_UOM']."\">");
                    print_r("<input type=\"text\" value=\"".$Devices[$x]['d_Role']."\">");
                    print_r("<input type=\"text\" value=\"".$Devices[$x]['d_Type']."\">");
                    print_r("<input type=\"text\" value=\"".$Devices[$x]['d_Address']."\">");
                    print_r("<input type=\"text\" value=\"".$Devices[$x]['d_Active']."\">");
                    
                    print_r("<br/>");
                } 
            ?>
            </div>
        </fieldset>
        <fieldset>
        <legend>User</legend>
		User Name: 
        <?php echo $_SESSION["User"] ?><br/>
        Role:
        <?php echo $group[0]["g_Name"] ?><br/>
        Update Password: <input type="password" name="SetUserPass"><br/>
        Confirm Password:<input type="password" name="SetUserPassConfirm">
        </fieldset>
        <input type="submit" value="Update">
		</form>
   	</body>
</html>
<?php
    include_once $path . "/Footer.php";
?>

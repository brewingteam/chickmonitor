<?php
    $path = $_SERVER['DOCUMENT_ROOT'];
    include_once $path . "/Header.php";

    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");

    $numWeeks = 1;
    if (isset($_POST['date'])) {
        $numWeeks = $_POST["date"];
    }

?> 
    <!--Load the AJAX API-->
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript">

        google.charts.load('current', {'packages':['bar']});
        //google.charts.load('current', {'packages':['annotationchart']});
        google.charts.load('current', {'packages':['calendar']});


        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawWeekDayChart);
        google.charts.setOnLoadCallback(drawFullChart);
        
        function drawWeekDayChart() {
            var EggData = $.ajax({
                type: "POST",
                url: "EggDataWeekDay.php",
                data:{date:"<?php echo $numWeeks ?>"},
                dataType: "json",
                async: false
            }).responseText;

            // Create our data table out of JSON data loaded from server.
            var EggDataTable = new google.visualization.DataTable(EggData);
            var EggOptions = {
                legend: { position : "none" },
                chart: {
                    title: 'count of eggs per Day of week',
                    axes: {
                        y: {
                            format: '0'
                        }
                    }
                }
            };
            var chart = new google.charts.Bar(document.getElementById('WeekDayChart_div'));
            chart.draw(EggDataTable, google.charts.Bar.convertOptions(EggOptions));
        }

        function drawFullChart() {
            var AllEggData = $.ajax({
                type: "POST",
                url: "EggDataAll.php",
                data:{date:"<?php echo $numWeeks ?>"},
                dataType: "json",
                async: false
            }).responseText;

            // Create our data table out of JSON data loaded from server.
            var AllEggDataTable = new google.visualization.DataTable(AllEggData);
            var AllEggOptions = {
                max: 5,
                min: 0,
                vAxis: {
                   format: '#'
                }
            };
            //var chart2 = new google.charts.Line(document.getElementById('AllEggChart_div'));
            var chart2 = new google.visualization.Calendar(document.getElementById('AllEggChart_div'));
            chart2.draw(AllEggDataTable, google.charts.Bar.convertOptions(AllEggOptions));
        }

    </script>
		

    </head>
        <body class="normal">
            <?php include_once "Nav.html"; ?>
            <div class="w3-row-padding">
                <form action="/TempChart.php" method="post">
                    Days (max 180):
                    <input type="number" name="date" min="0" max="180" value="<?php echo $numWeeks ?>">
                      <input type="submit">
                </form>
            </div>
            <div id="dashboard">
                <div id="WeekDayChart_div" style="width: 95vw; height: 90vh; margin: 0 auto; "></div>
                <div id="AllEggChart_div" style="width: 95vw; height: 90vh; margin: 0 auto; "></div>
                <div id="range_filter_div"></div>
            </div>
        </body>
    </html>
    <?php
    include_once $path . "/Footer.php";
    ?>
    

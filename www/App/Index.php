
<?php

$path = $_SERVER['DOCUMENT_ROOT'];
include "./Header.php"

?>

</head>
   <body class="normal">
   <div class="loader">
    <svg viewBox="0 0 32 32" width="32" height="32">
      <circle id="spinner" cx="16" cy="16" r="14" fill="none"></circle>
    </svg>
  <header class="header">
    <h1 class="header__title">Coop Details</h1>
    <button id="butRefresh" class="headerButton" aria-label="Refresh"></button>
  </header>

  <main class="main">
    <div class="card cardTemplate weather-forecast">
      <div class="Name"></div>
      <div class="card-last-updated"></div>
      <div class="Temperature"></div>
      <div class="Humidity"></div>
    </div>
  </main>

  <div class="loader">
    <svg viewBox="0 0 32 32" width="32" height="32">
      <circle id="spinner" cx="16" cy="16" r="14" fill="none"></circle>
    </svg>
  </div>
  <script src="./Scripts/Main.js"></script>

<?php

include "./Footer.php"

?>
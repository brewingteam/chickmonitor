<?php

$device = $_POST["device"];

$creds = parse_ini_file("/home/pi/secure.cfg",true);

//print_r($creds["database"]["dbusername"]);

// Create connection
$conn = new mysqli($creds["database"]["host"], $creds["database"]["dbusername"], $creds["database"]["dbpassword"], $creds["database"]["dbname"]);

	// Check connection
	if($conn->connect_errno > 0){
		die('Unable to connect to database [' . $conn->connect_error . ']');
	}
    if ($device!=''){
        $sql    = "SELECT d.d_Name, t.t_Temp FROM TEMPS t join DEVICES d on t.t_device = d.d_device WHERE d.d_Name like '".$device."' AND d.d_Active = 1";
    }else{
        $sql    = "SELECT d.d_Name, t.t_Temp FROM TEMPS t join DEVICES d on t.t_device = d.d_device WHERE d.d_Role in ('OutsideTemp','CoopTemp') AND d.d_Active = 1";
    }
		// Perform queries 
	if(!$Current_Temp_Records = $conn->query($sql)){
		die('There was an error running the query [' . $conn->error . ']');
	}
	
	$jsonString = '{
  "cols": [
        {"label":"Name","type":"string"},
		{"label":"temperature","type":"number"}
      ],
  "rows": [';
  
	while($row = $Current_Temp_Records->fetch_assoc()){
		$jsonString .= '{"c":[{"v":"'.$row['d_Name'].'"},{"v":'.$row['t_Temp'].'}]},';

	
	}
	
$jsonString = substr($jsonString,0,-1);
$jsonString  .= ']}';

$Current_Temp_Records->free();
$conn->close();

echo $jsonString;
?>

var CacheName = 'BaseCache-V2.25';
var DataCacheName = 'ChickData-V1';
var CoreFiles = [
  './',
  './Index.php',
  './Header.php',
  './Scripts/Main.js',
  './Styles/ChickStyle.css',
  './Images/Chicken_closeup.png',
  './Images/128px-Chicken_closeup.png',
  './Images/64px-Chicken_closeup.png',
  './Images/32px-Chicken_closeup.png',
  './Images/16px-Chicken_closeup.png',
];

self.addEventListener('install', function(e){
  e.waitUntil(
    caches.open(CacheName).then(function(cache){
      console.log('[ServiceWorker] Caching App shell');
      return cache.addAll(CoreFiles);
    })
  );
});

self.addEventListener('activate', function(e){
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keylist){
      return Promise.all(keylist.map(function(key){
        if (key !== CacheName) {
          console.log('[ServiceWorker] Remove Old Cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
});

self.addEventListener('fetch', function(e){
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    caches.match(e.request).then(function(response){
      return response || fetch(e.request);
    })
  );
});
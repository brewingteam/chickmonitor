(function() {
  'use strict';

  var app = {
    isLoading: true,
    visibleCards: {},
    selectedCities: [],
    spinner: document.querySelector('.loader'),
    cardTemplate: document.querySelector('.cardTemplate'),
    container: document.querySelector('.main'),
    addDialog: document.querySelector('.dialog-container'),
    daysOfWeek: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
  };

  var initialTemps = {
    Device: 'CoopTemp',
    Created: '2016-07-22T01:00:00Z',
    Details: {
        Temperature: 75,
        Humidity: 50
      },
    };

  /*****************************************************************************
   *
   * Event listeners for UI elements
   *
   ****************************************************************************/

  document.getElementById('butRefresh').addEventListener('click', function() {
    // Refresh all of the forecasts
    app.updateTemps();
  });
    
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('ServiceWorker.js')
    .then(function(registration) {
      console.log('Registration successful, scope is:', registration.scope);
    })
    .catch(function(error) {
      console.log('Service worker registration failed, error:', error);
    });
  }

  app.saveTemps = function() {
    var Temps = JSON.stringify(app.Temps);
    localStorage.Temps = Temps;
  };

  app.updateTempsCard = function(data) {
    var Name = data.Device;
    var dataLastUpdated = new Date(data.Created);
    var Temperature = data.Details.Temperature;
    var humidity = data.Details.Humidity;

    var card = app.visibleCards[data.key];
    if (!card) {
      card = app.cardTemplate.cloneNode(true);
      card.classList.remove('cardTemplate');
      card.querySelector('.Name').textContent = data.Device;
      card.removeAttribute('hidden');
      app.container.appendChild(card);
      app.visibleCards[data.key] = card;
    }

    // Verifies the data provide is newer than what's already visible
    // on the card, if it's not bail, if it is, continue and update the
    // time saved in the card
    var cardLastUpdatedElem = card.querySelector('.card-last-updated');
    var cardLastUpdated = cardLastUpdatedElem.textContent;
    if (cardLastUpdated) {
      cardLastUpdated = new Date(cardLastUpdated);
      // Bail if the card has more recent data then the data
      if (dataLastUpdated.getTime() < cardLastUpdated.getTime()) {
        return;
      }
    }
    cardLastUpdatedElem.textContent = data.Created;

    card.querySelector('.Name').textContent = Name;
    card.querySelector('.Temperature').textContent = Math.round(Temperature) + '%';
    card.querySelector('.Humidity').textContent = Math.round(humidity) + '°';

    
    if (app.isLoading) {
      app.spinner.setAttribute('hidden', true);
      app.container.removeAttribute('hidden');
      app.isLoading = false;
    }
  };

  app.getTemps = function(key, label) {
    var url = 'https://scubapi.duckdns.org/App/Data/Data_CurrentTemps.php?device=' + key;
    // TODO add cache logic here
    if ('caches' in window) {
      /*
       * Check if the service worker has already cached this city's weather
       * data. If the service worker has the data, then display the cached
       * data while the app fetches the latest data.
       */
      caches.match(url).then(function(response) {
        if (response) {
          response.json().then(function updateFromCache(json) {
            var results = json.query.results;
            results.key = key;
            results.label = label;
            results.created = json.query.created;
            app.updateTempsCard(results);
          });
        }
      });
    }
    // Fetch the latest data.
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState === XMLHttpRequest.DONE) {
        if (request.status === 200) {
          var response = JSON.parse(request.response);
          var results = response.query.results;
          results.key = key;
          results.label = label;
          results.created = response.query.created;
          app.updateTempsCard(results);
        }
      } else {
        // Return the initial weather forecast since no data is available.
        app.updateTempsCard(initialTemps);
      }
    };
    request.open('GET', url);
    request.send();
  };

  app.updateTempsCard(initialTemps);
  app.Temps = [
    {key: initialTemps.key, label: initialTemps.label}
  ];
  app.saveTemps();
})();

<?php
    #uncomment the following 2 lines to debug PHP
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
   
    $creds = parse_ini_file("/home/pi/secure.cfg",true);
    
    //print_r($creds["database"]["dbusername"]);

    // Create connection
    $conn = new mysqli($creds["database"]["host"], $creds["database"]["dbusername"], $creds["database"]["dbpassword"], $creds["database"]["dbname"]);

    // Check connection
    if($conn->connect_errno > 0){
        die('Unable to connect to database [' . $conn->connect_error . ']');
    }

    function dbQuery($fquery){
        global $conn;
        if(!$results = $conn->query($fquery)){
            die('There was an error running the query [' . $conn->error . ']');
            return "ERROR";
        }
        ####need code here to check for BOOL results on UPDATE/INSERT queries.
        #return mysqli_fetch_array($results);
        return mysqli_fetch_all($results,MYSQLI_ASSOC);
    }
    
    $query = "SELECT * from DETAILS where d_ID = \"Site\" AND d_Attribute = \"Name\"";
    if(!$name = $conn->query($query)){
        die('There was an error running the query [' . $conn->error . ']');
    }
    $SiteName = mysqli_fetch_array($name);
    
?>

<!DOCTYPE html>
<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $SiteName["d_Value"];?> </title>
    <link rel="icon" href="/images/32px-Chicken_closeup.png">
	<link rel="stylesheet" href="/Style.css">

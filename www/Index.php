<?php

$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . "/Header.php";


$maxq = "select MAX(t.t_Temp) as \"max\", MIN(t.t_Temp) as \"min\" from TEMPS_LOG t where t.t_Name = \"OutsideTemp\" AND DATE(t.t_DateTime) = DATE(NOW())";

if(!$max = $conn->query($maxq)){
    die('There was an error running the query [' . $conn->error . ']');
}
$maxtemp = mysqli_fetch_array($max);

$amaxq = "select MAX(t.t_Temp) as \"max\", MIN(t.t_Temp) as \"min\" from TEMPS_LOG t where t.t_Name = \"OutsideTemp\"";

if(!$amax = $conn->query($amaxq)){
    die('There was an error running the query [' . $conn->error . ']');
}
$amaxtemp = mysqli_fetch_array($amax);

$coopdq = "select MAX(t.t_Temp) as \"max\", MIN(t.t_Temp) as \"min\" from TEMPS_LOG t where t.t_Name = \"CoopTemp\" AND DATE(t.t_DateTime) = DATE(NOW())";

if(!$cdaily = $conn->query($coopdq)){
    die('There was an error running the query [' . $conn->error . ']');
}
$aCoopDaily = mysqli_fetch_array($cdaily);

$coopallq = "select MAX(t.t_Temp) as \"max\", MIN(t.t_Temp) as \"min\" from TEMPS_LOG t where t.t_Name = \"CoopTemp\"";

if(!$coopall = $conn->query($coopallq)){
    die('There was an error running the query [' . $conn->error . ']');
}
$aCoopAll = mysqli_fetch_array($coopall);


$DOBq = "select d_Value from DETAILS where d_Attribute = \"DOB\" and d_ID=\"Chicks\"";

if(!$DOBr = $conn->query($DOBq)){
    die('There was an error running the query [' . $conn->error . ']');
}
$DOB = mysqli_fetch_array($DOBr);

$eggq = "select d_Value from DETAILS where d_Attribute = \"Eggs\" and d_ID=\"Chicks\"";

if(!$eggr = $conn->query($eggq)){
    die('There was an error running the query [' . $conn->error . ']');
}
$eggs = mysqli_fetch_array($eggr);

$HeaterQuery = "Select * from TEMPS where t_Device = 'GPIO14'";
if(!$Heater = $conn->query($HeaterQuery)){
    die('There was a problem getting the heater info' . $conn->error);
}
$HeaterState = mysqli_fetch_array($Heater);

$WunderAPIQuery = "select * from DETAILS where d_ID = 'Wunderground' and d_Attribute = 'APIkey'";
if(!$WunderResult = $conn->query($WunderAPIQuery)){
    die('There was a problem getting the heater info' . $conn->error);
}
$WundergroundAPI = mysqli_fetch_array($WunderResult);

$LocationQuery = "Select * from DETAILS where d_ID = 'Location' and d_Attribute = 'City'";
if(!$LocationResult = $conn->query($LocationQuery)){
    die('There was a problem getting the heater info' . $conn->error);
}
$location = mysqli_fetch_array($LocationResult);

$heaterEnabled = dbQuery("Select * from DETAILS where d_ID = 'WaterHeater' and d_Attribute = 'Enabled'");
$heaterEnabled = $heaterEnabled[0]['d_Value'];

$heaterOn = dbQuery("SELECT T.t_Temp FROM ChickTemps.TEMPS T LEFT JOIN DEVICES D ON T.t_Device = D.d_Device WHERE D.d_Role = 'WaterHeater';");
$heaterOn = $heaterOn[0]['t_Temp'];

$HeatLow = dbQuery("SELECT D.d_Value FROM ChickTemps.DETAILS D WHERE D.d_ID = 'WaterHeater' AND D.d_Attribute = 'MinTemp';");
$HeatLow = $HeatLow[0]['d_Value'];

$HeatTo = dbQuery("SELECT D.d_Value FROM ChickTemps.DETAILS D WHERE D.d_ID = 'WaterHeater' AND D.d_Attribute = 'HeatTo';");
$HeatTo = $HeatTo[0]['d_Value'];

$Expense = dbQuery("select SUM(e_Ammount) TotalCost FROM ChickTemps.EXPENSES");
$Expense = $Expense[0]['TotalCost'];

$EggTotal = dbQuery("SELECT Count(E.e_ID) as \"EggCount\" FROM ChickTemps.EGGS E;");
$EggTotal = $EggTotal[0]['EggCount'];

$EggWeek = dbQuery("SELECT Count(E.e_ID) as \"EggCount\" FROM ChickTemps.EGGS E WHERE DATE(E.e_DateTime) >= date_add(CurDate(),INTERVAL - (dayofweek(CurDate())-1) day);");
$EggWeek = $EggWeek[0]['EggCount'];

$EggCost = dbQuery("select EXP.TotalCost, EGG.TotalEggs, ROUND(EXP.TotalCost/EGG.TotalEggs,3) as CostPerEgg from (select SUM(e_Ammount) TotalCost FROM ChickTemps.EXPENSES) EXP, (Select count(e_ID) TotalEggs FROM ChickTemps.EGGS) EGG;");
$EggCost = round($EggCost[0]['CostPerEgg'],2);

$FeedLimits = dbQuery("select D.d_Attribute, D.d_Value from ChickTemps.DETAILS D where D.d_ID = 'Feed';");
$FeedLimitHigh=$FeedLimits[1]['d_Value'];
$FeedLimitLow=$FeedLimits[0]['d_Value'];

$FeedFull = $FeedLimitHigh - $FeedLimitLow;
$FeedLevel = dbQuery("select t.t_Temp as FeedLevel from ChickTemps.TEMPS t join ChickTemps.DEVICES d on t.t_Device = d.d_Device where d.d_Name = 'FeedLevel';");
$FeedLevel = $FeedLevel[0]["FeedLevel"];
$FeedPercent = (($FeedLimitHigh - $FeedLevel) / $FeedFull) * 100;

?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
    <!-- load firebase scripts -->
    <!-- Firebase App is always required and must be first -->
    <script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-app.js"></script>

    <!-- Add additional services that you want to use -->
        <script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-database.js"></script>
        <script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-firestore.js"></script>
        <script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-functions.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.7.2/firebase-messaging.js"></script>
    

    
    <script type="text/javascript">
    var mylocation="";
    var p_date="";
    var  high_f="";
    var  low_f="";
    var  humid="";
    var  condi="";
    var  tempF="";
    
        google.charts.load('current', {'packages':['gauge']});
        google.charts.setOnLoadCallback(drawChart);
        
        function checkTime(i) {
            if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
            return i;
        }
        function getData(){
            var jsonData = $.ajax({
            url: "CurrentData.php",
            dataType: "json",
            async: false
            }).responseText;

            // Create our data table out of JSON data loaded from server.
            var gdata = new google.visualization.DataTable(jsonData);

            return gdata;
        }
        function drawChart() {
        
            var data = getData();
            
            var options = {
                //width: 600, 
                height: 200,
                redFrom: 100, redTo: 120,
                yellowFrom:-20, yellowTo: -10,
                greenFrom: 10, greenTo: 90,
                minorTicks: 5,
                max:120, min:-20,
                animation: {
                duration: 5000,
                easing: 'inAndOut'
              }
            };

            var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

              var DummyData = google.visualization.arrayToDataTable([
              ['Label', 'Value'],
              ['CoopTemp', 50],
              ['OutsideTemp', 50],
            ]);
             chart.draw(DummyData, options);
             console.log("drawing chart with:", data)
            chart.draw(data, options);
            
            setInterval(function() {
                var data = getData();
                chart.draw(data, options);
                
                //set last_update time 
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                document.getElementById('last_update').innerHTML = "last update: " + h + ":" + m + ":" + s;
            }, 30000);
        
        }
        
        
            google.charts.setOnLoadCallback(drawHumidChart);
            function getHumidData(){
                var jsonData = $.ajax({
                type: "POST",
                url: "CurrentData.php",
                data:{device:"%Humidity"},
                dataType: "json",
                async: false
                }).responseText;

                // Create our data table out of JSON data loaded from server.
                var gdata = new google.visualization.DataTable(jsonData);

                return gdata;
            }
            function drawHumidChart() {

                var dataHumid = getHumidData();

                var optionsHumid = {
                    //width: 600,
                    height: 200,
                    greenFrom: 20, greenTo: 65,
                    yellowFrom:65, yellowTo: 80,
                    redFrom: 80, redTo: 100,
                    minorTicks: 5,
                    min:0, max:100,
                    animation: {
                        duration: 5000,
                        easing: 'inAndOut'
                    }
                };

                var chartHumid = new google.visualization.Gauge(document.getElementById('chart_Humid'));
                
                var DummyHData = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['CoopHumidity', 50],
                    ['OutsideHumidity', 50],
                ]);
                 
                chartHumid.draw(DummyHData, optionsHumid); 
                chartHumid.draw(dataHumid, optionsHumid);
                //$("#HumidOverlay").html("<img src='/images/GrayStatus.png' style='width:32px;height:14px;'alt='Heater:ON' />");
            }
        
        
            //create water heater gauge
            google.charts.setOnLoadCallback(drawWaterChart);
            function getWaterData(){
                var jsonData = $.ajax({
                type: "POST",
                url: "CurrentData.php",
                data:{device:"WaterTemp"},
                dataType: "json",
                async:false
                }).responseText;

                // Create our data table out of JSON data loaded from server.
                var gdata = new google.visualization.DataTable(jsonData);

                return gdata;
            }
            function drawWaterChart() {

                var data2 = getWaterData();

                var options2 = {
                    //width: 600,
                    height: 200,
                    redFrom: 20, redTo: 32,
                    yellowFrom:<?php echo $HeatLow;?>, yellowTo: <?php echo $HeatTo;?>,
                    greenFrom: <?php echo (int)$HeatTo + 1;?>, greenTo: 100,
                    minorTicks: 5,
                    max:100, min:20,
                    animation: {
                        duration: 5000,
                        easing: 'inAndOut'
                    }
                };

                var WaterChart = new google.visualization.Gauge(document.getElementById('chart_Water'));
                
                var DummyWData = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['WaterTemp', 60],
                ]);
                
                WaterChart.draw(DummyWData, options2);
                WaterChart.draw(data2, options2);
                <?php if ($heaterEnabled ==1){ 
                    if ($heaterOn == 1){ ?>
                        $("#overlay").html("<img src='/images/Red_Light_Icon.png' style='width:30px;height:30px;'alt='Heater:ON' />");
                    <?php }else{ ?>
                        $("#overlay").html("<img src='/images/Gray_Light_Icon.png' style='width:30px;height:30px;' alt='Heater:OFF' />");
                    <?php } }?>
            }
            google.charts.setOnLoadCallback(drawFeedChart);
            function drawFeedChart() {

                var FeedOptions = {
                    //width: 600,
                    height: 200,
                    redFrom: 0, redTo: 25,
                    yellowFrom: 26, yellowTo: 50,
                    greenFrom: 51, greenTo: 100,
                    minorTicks: 5,
                    max: 100, min:0,
                    animation: {
                        duration: 5000,
                        easing: 'inAndOut'
                    }
                };

                var FeedChart = new google.visualization.Gauge(document.getElementById('chart_Feed'));

                var DummyFData = google.visualization.arrayToDataTable([
                    ['Label', 'Value'],
                    ['FeedLevel', <?php echo $FeedPercent ?>],
                ]);
                console.log("drawing FeedChart with:", DummyFData)
                FeedChart.draw(DummyFData, FeedOptions);
                //FeedChart.draw(data2, FeedOptions);
            }
    </script>

</head>
    <body class="normal">
        <?php include_once "Nav.html"; ?>
        
        <a class="noSelect" href="javascript:drawChart();"><div id="chart_div"></div></a>
        <a href="javascript:drawHumidChart();"><div class="ExtraCharts">
            <div class="chart_Water" id="chart_Humid"></div>
            <div class="Overlay" id="HumidOverlay"></div>
        </div></a>
        <a href="javascript:drawWaterChart();"><div class="chartWithOverlay">
            <div id="chart_Water"></div>
            <div class="Overlay" id="overlay"></div>
        </div></a>
        <a class="noSelect" href="javascript:drawFeedChart();"><div id="chart_Feed"></div></a>
        
        <div id="last_update"></div>
        <div>
            <p class="boxed">
            <b>Chick Info:</b><br/>
            <?php
                $time_since = date_diff(date_create(date("Y/m/d",strtotime($DOB["d_Value"]))),date_create(date("Y/m/d")));
                $weeks_since = round($time_since->format("%a") /7,1);
                echo "DOB: " . $DOB["d_Value"];
                echo " (" . $time_since->format("%y Years %m months %d Days") . ")";
                echo "(" . $weeks_since . " weeks)";
            ?>
            <br/>
            Total Cost: $<?php echo $Expense ?></br>
            <?php if ($EggTotal > 0) { ?>
                Total Eggs: <?php echo $EggTotal ?> <br/>
               
            <?php }else{ ?>
                Expected Eggs: <?php echo $eggs["d_Value"]?> (<?php echo date_diff(date_create(date("Y/m/d",strtotime($eggs["d_Value"]))),date_create(date("Y/m/d")))->format("%m months %d Days")?>)
            <?php } ?>
            </p>
        </div>
        <div>
            <p class="boxed">
            <b>Stats</b><br/>
            Egg Cost:   $<?php echo $EggCost ?>/egg or $<?php echo $EggCost*12 ?>/dz</br>
            Weeks Eggs: <?php echo $EggWeek ?></br>
            Feed Level: <?php echo $FeedLevel ?>cm (<?php echo $FeedPercent ?>%)</br>
        </div>

        <div>
            <p class="boxed" id="forecast">
                <b>Forcast: (WeatherUnderground has shutdown their API)</b>
            </p>
        </div>
        <div>
            <p class="boxed" id="forecast">
                <b>ChickPi Records</b><br/>
                Today: Outside:<a style="color:red;"><?php echo $maxtemp["max"] ?> </a>/ <a style="color:blue;"><?php echo $maxtemp["min"] ?></a> Coop: <a style="color:red;"><?php echo $aCoopDaily["max"] ?> </a>/ <a style="color:blue;"><?php echo $aCoopDaily["min"] ?></a><br /><br/>
                Ever : Outside:<a style="color:red;"><?php echo $amaxtemp["max"] ?> </a>/ <a style="color:blue;"><?php echo $amaxtemp["min"] ?></a> Coop: <a style="color:red;"><?php echo $aCoopAll["max"] ?> </a>/ <a style="color:blue;"><?php echo $aCoopAll["min"] ?></a><br />
            </p>
        <div>
    </body>
</html>
<?php
    include_once $path . '/Footer.php';
?>

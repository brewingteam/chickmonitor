<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$reqdate = $_POST["date"];
$device = $_POST["device"];

if (!$reqdate){
    $reqdate = 10;
}
if (!$device){
    $device = 'FeedLevel';
}

$creds = parse_ini_file("/home/pi/secure.cfg",true);

//print_r($creds["database"]["dbusername"]);

// Create connection
$conn = new mysqli($creds["database"]["host"], $creds["database"]["dbusername"], $creds["database"]["dbpassword"], $creds["database"]["dbname"]);

// Check connection
if($conn->connect_errno > 0){
    die('Unable to connect to database [' . $conn->connect_error . ']');
}

$LimitsQuery = "select D.d_Attribute, D.d_Value from ChickTemps.DETAILS D where D.d_ID = 'Feed';";
if(!$FeedLimitResult = $conn->query($LimitsQuery)){
	die(print_r('There was an error running the query [' . $conn->error . ']'));
}else{
    $FeedLimits = mysqli_fetch_all($FeedLimitResult,MYSQLI_ASSOC);
}

#$FeedLimits = dbQuery("select D.d_Attribute, D.d_Value from ChickTemps.DETAILS D where D.d_ID = 'Feed';");
$FeedLimitHigh=$FeedLimits[1]['d_Value'];
$FeedLimitLow=$FeedLimits[0]['d_Value'];

$FeedFull = $FeedLimitHigh - $FeedLimitLow;


//simple query for ReqDate
$query = "SELECT 
    DATE_FORMAT(L.t_DateTime, '%Y,%c,%d,%H,%i') date,
    L.t_Temp Temp
FROM
    TEMPS_LOG L
WHERE
	L.t_Name = '".$device."'
    AND
    L.t_DateTime >= (CURDATE() - INTERVAL ".$reqdate." DAY)
ORDER BY L.t_DateTime ASC";

//print_r($query);
//$query = "SELECT t_DateTime, t_Temp from TEMPS_LOG WHERE t_DateTime >= (CURDATE() - INTERVAL ".$reqdate." DAY) order by t_DateTime ASC";
// Perform queries 
if(!$result = $conn->query($query)){
	die(print_r('There was an error running the query [' . $conn->error . ']'));
}
#{"label":"Device","type":"string"},
$jsonString = '{
	"metadata" :{
        "msg": "requested '.$reqdate.':'.$device.'"
    },
  "cols": [
        {"label":"Date","type":"datetime"},
		{"label":"'.$device.'","type":"number"}
      ],
  "rows": [';

#while ($row = mysqli_fetch_array($result, MYSQL_NUM)) {
while ($row = $result->fetch_array()) {
	preg_match('/(\d{4}),(\d+),(\d+),(\d+),(\d+)/i',$row[0], $matches);
	$correctMonth = (int)$matches[2] - 1;
	$corrected = (string)$matches[1].",".(string)$correctMonth.",".(string)$matches[3].",".(string)$matches[4].",".(string)$matches[5];
    
    #change to % based on limits
    if (is_numeric ($row[1])){
        $FeedPercent = (($FeedLimitHigh - $row[1]) / $FeedFull) * 100;
    }else{
        $FeedPercent = 'null';
    }
    
	#$jsonString .= '{"c":[{"v":"Date('.$corrected.')"},{"v":'.(is_numeric ($row[1])?$row[1]:'null').'},';
	 $jsonString .= '{"c":[{"v":"Date('.$corrected.')"},{"v":'.$FeedPercent.'}]},';
	#$jsonString .= '{"c":[{"v":"'.$row[0].'"},{"v":"'.$row[1].'"}]},';
	#$jsonString .= '{"c":[{"v":"Date('.$row[0].')"},{"v":'.$row[1].'},{"v":"'.$row[2].'"},{"v":"'.$row[3].'"},{"v":'.(is_numeric ($row[4])?$row[4]:'null').'},{"v":null},{"v":null}]},';
}
if (substr($jsonString, -1) != "["){
    $jsonString = substr($jsonString,0,-1);
}

$jsonString  .= ']}';


echo $jsonString;
?>

<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . "/Header.php";

$size = dbQuery("SELECT table_schema, Round(Sum(data_length + index_length) / 1024 / 1024, 1) \"Size\" FROM information_schema.tables GROUP BY table_schema");
$TEMPS_LOG_COUNT = dbQuery("select count(*) \"count\" from TEMPS_LOG");
$TEMPS_LOG_AGE = dbQuery("SELECT L.t_DateTime FROM TEMPS_LOG L ORDER BY L.t_DateTime ASC LIMIT 1");
$dbversion = dbQuery("SHOW VARIABLES LIKE \"%innodb_version%\"");

$time_since = date_diff(date_create(date("Y/m/d",strtotime($TEMPS_LOG_AGE[0]["t_DateTime"]))),date_create(date("Y/m/d")));

$str   = @file_get_contents('/proc/uptime');
$num   = floatval($str);
$secs  = fmod($num, 60); $num = intdiv($num, 60);
$mins  = $num % 60;      $num = intdiv($num, 60);
$hours = $num % 24;      $num = intdiv($num, 24);
$days  = $num;

$df = disk_free_space("/");

$df = $df/1024/1024
?>

</head>
	<body class="normal">
		<?php include_once $path ."/Nav.html"; ?>
		
    <audio autoplay> 
        <source src="/audio/Baby_Chick_Chirp.mp3">
    </audio>
		
		<div class="container">
			<img src="images/YoungChicks.jpg">
            <div class="top-left">
                Just a little project for the Raspberry pi.
                
                <div>
                    <fieldset>
                        <legend>System Stats</legend>
                        <?php 
                            print_r("DB is: ". $size[0]['Size']." MB<br/>");
                            print_r("TEMPS_LOG record count is:". $TEMPS_LOG_COUNT[0]['count']."<br/>");
                            print_r("Oldest TEMPS_LOG record is:". $TEMPS_LOG_AGE[0]['t_DateTime']. " (" . $time_since->format("%y Years %m months %d Days") . ")<br/>");
                            print_r("System uptime:". $days. " days ".$hours." hours.<br/>");
                            print_r("Disk Free Space:". $df. " MB<br/>");
                            
                        ?>
                        <br/>
                    </fieldset>
                    <fieldset>
                        <legend>Versions</legend>
                        <?PHP 
                            echo 'PHP: ' . phpversion(). '<br/>';
                            echo apache_get_version().'<br/>';
                            echo 'MySQL: ' . $dbversion[0]['Value'].'<br/>';
                        ?>
                    </Fieldset>
                </div>
            </div>
		</div>
		
	</body>
</html>
<?php
    include_once $path . "/Footer.php";
?>
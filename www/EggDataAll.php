<?php

$reqdate = $_POST["date"];

if (!$reqdate)
{
	$reqdate = 1;
}

$creds = parse_ini_file("/home/pi/secure.cfg",true);

//print_r($creds["database"]["dbusername"]);

// Create connection
$conn = new mysqli($creds["database"]["host"], $creds["database"]["dbusername"], $creds["database"]["dbpassword"], $creds["database"]["dbname"]);

// Check connection
if($conn->connect_errno > 0){
    die('Unable to connect to database [' . $conn->connect_error . ']');
}

//massive join super slow.
//$query = "Select DATE_FORMAT(t.t_DateTime,'%Y,%c,%d,%H,%i'), t.t_Temp, t2.t_Temp From TEMPS_LOG t Left Join TEMPS_LOG t2 on substring(t2.t_DateTime,1,length(t2.t_DateTime)-3) = substring(t.t_DateTime,1,length(t.t_DateTime)-3)  and t2.t_Device='/sys/bus/w1/devices/28-00000534a0d9' Where t.t_Device ='/sys/bus/w1/devices/28-00000534c954' order by t.t_DateTime asc";

//simple query for ReqDate
$query = "select DATE_FORMAT(E.e_DateTime, '%Y,%c,%d') date, count(*) eggs from ChickTemps.EGGS E group by date(E.e_DateTime)";

//print_r($query);
//$query = "SELECT t_DateTime, t_Temp from TEMPS_LOG WHERE t_DateTime >= (CURDATE() - INTERVAL ".$reqdate." DAY) order by t_DateTime ASC";
// Perform queries 
if(!$result = $conn->query($query)){
	die(print_r('There was an error running the query [' . $conn->error . ']'));
}
#{"label":"Device","type":"string"},
$jsonString = '{
	"metadata" :{
        "msg": "requested '.$reqdate.'"
    },
  "cols": [
        {"label":"Date","type":"datetime"},
		{"label":"Eggs","type":"number"}
      ],
  "rows": [';

#while ($row = mysqli_fetch_array($result, MYSQL_NUM)) {
while ($row = $result->fetch_array()) {
    preg_match('/(\d{4}),(\d+),(\d+)/i',$row[0], $matches);
    //preg_match('/(\d{4}),(\d+),(\d+),(\d+),(\d+)/i',$row[0], $matches);
    $correctMonth = (int)$matches[2] - 1;
    $corrected = (string)$matches[1].",".(string)$correctMonth.",".(string)$matches[3].",".(string)$matches[4].",".(string)$matches[5];
    
    $jsonString .= '{"c":[{"v":"Date('.$corrected.')"},{"v":'.(is_numeric ($row[1])?$row[1]:'null').'}]},';
	//$jsonString .= '{"c":[{"v":"'.$row[0].'"},{"v":'.(is_numeric ($row[1])?$row[1]:'null').'}]},';
}
$jsonString = substr($jsonString,0,-1);

$jsonString  .= ']}';


echo $jsonString;
?>

<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . "/Header.php";

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$numdays = 1;
if( isset($_POST['date']) )
{
	$numdays = $_POST["date"];
}

$heaterEnabled = dbQuery("Select * from DEVICES where d_Role = 'WaterTemp' and d_Active = 1");
$heaterEnabled = $heaterEnabled[0]['d_Active'];

?> 
		<!--Load the AJAX API-->
		<script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript">

			// Load the Visualization API and the corechart package.
			//google.charts.load('current', {'packages':['corechart']});
			//google.charts.load('visualization', '1', {packages: ['controls']});
			google.charts.load('current', {'packages':['line', 'corechart']});
			
			// Set a callback to run when the Google Visualization API is loaded.
			google.charts.setOnLoadCallback(drawChart);

			
			function drawChart() {
				var OutsideTempData = $.ajax({
					type: "POST",
					url: "TempData.php",
					data:{date:"<?php echo $numdays ?>",device:"OutsideTemp"},
					dataType: "json",
					async: false
				}).responseText;
				
				var CoopTempData = $.ajax({
					type: "POST",
					url: "TempData.php",
					data:{date:"<?php echo $numdays ?>",device:"CoopTemp"},
					dataType: "json",
					async: false
				}).responseText;
				
				var CoopHumidityData = $.ajax({
					type: "POST",
					url: "TempData.php",
					data:{date:"<?php echo $numdays ?>",device:"CoopHumidity"},
					dataType: "json",
					async: false
				}).responseText;
				
				var OutsideHumidityData = $.ajax({
					type: "POST",
					url: "TempData.php",
					data:{date:"<?php echo $numdays ?>",device:"OutsideHumidity"},
					dataType: "json",
					async: false
				}).responseText;
				
                                       
				// Create our data table out of JSON data loaded from server.
				var OutsideTempDataTable = new google.visualization.DataTable(OutsideTempData);
				var CoopTempDataTable = new google.visualization.DataTable(CoopTempData);
				var CoopHumidityDataTable = new google.visualization.DataTable(CoopHumidityData);
				var OutsideHumidityDataTable = new google.visualization.DataTable(OutsideHumidityData);
				
				
				var OutsideInsideTemp = google.visualization.data.join(OutsideTempDataTable, CoopTempDataTable, 'full', [[0, 0]], [1], [1]);
				var OutsideInsideHumidity = google.visualization.data.join(CoopHumidityDataTable, OutsideHumidityDataTable, 'full', [[0, 0]], [1], [1]);
				var WeatherData = google.visualization.data.join(OutsideInsideTemp, OutsideInsideHumidity, 'full', [[0, 0]], [1,2], [1,2]);
				
                <?php if ($heaterEnabled ==1){ ?>
                    var WaterTempData = $.ajax({
                        type: "POST",
                        url: "TempData.php",
                        data:{date:"<?php echo $numdays ?>",device:"WaterTemp"},
                        dataType: "json",
                        async: false
                    }).responseText;
                    
                    var HeaterData = $.ajax({
                        type: "POST",
                        url: "TempData.php",
                        data:{date:"<?php echo $numdays ?>",device:"WaterHeater"},
                        dataType: "json",
                        async: false
                    }).responseText;
                    
                    try{
                        var WaterTempDataTable = new google.visualization.DataTable(WaterTempData);
                        var OutsideWaterData = google.visualization.data.join(OutsideTempDataTable, WaterTempDataTable, 'full', [[0, 0]], [1], [1]);
                    }catch(err){
                        OutsideWaterData = "ERROR";
                    }
                    
                    try{                    
                        var HeaterDataTable = new google.visualization.DataTable(HeaterData);
                        var OutsideWaterHeaterData = google.visualization.data.join(OutsideWaterData, HeaterDataTable, 'full', [[0, 0]], [1,2], [1]);
                    }catch(err) {
                        OutsideWaterHeaterData = "ERROR";
                    }
                     
                <?php }
                ?>
                
				var InsideOutsideOptions = {
					title: 'Coop vs. Outside Temp',
					curveType: 'function',
					legend: { position: 'bottom' },
					interpolateNulls: true,
					series: {
						// Gives each series an axis name that matches the Y-axis below.
						0: {targetAxisIndex: 1, color: 'black'},
						1: {targetAxisIndex: 1, color: 'green'},
						2: {targetAxisIndex: 0, color: 'orange'},
						3: {targetAxisIndex: 0, color: 'yellow'}
					},
					vAxes: {
						// Adds labels to each axis; they don't have to match the axis names.
						0: {title: 'Humidity (%)',
								//format: 'percent',
								maxValue: 100,
								minValue: 0,
							},
						1: {title: 'Temp (°F)'}
						
						
					},
					trendlines: {
						1: {
							type: 'polynomial',
							degree: 10,
							pointsVisible: false,
							labelInLegend: 'Average Coop Temp',
							visibleInLegend: true,
						},						
					}
				};
                 // added a second dummy axis simply to move the label the right of the chart.
                var WaterOptions = {
					title: 'Water vs. Outside Temp',
					curveType: 'function',
					legend: { position: 'bottom' },
					interpolateNulls: true,
					seriesType: 'line',
					series: {
						// Gives each series an axis name that matches the Y-axis below.
						0: {targetAxisIndex: 1,
                            				color: 'black'},
						1: {targetAxisIndex: 1,
                            				color: 'blue'},
						2: {targetAxisIndex: 0,
							color: 'red',
							type: 'area'
                            }
					},
                    vAxes: {
						// Adds labels to each axis; they don't have to match the axis names.
						0: {title: 'Off/On',
							maxValue: 1,
							minValue: 0,
						},
						1: {title: 'Temp (°F)'},
                    }
				};
				
				var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
				chart.draw(WeatherData, InsideOutsideOptions);
                
                <?php if ($heaterEnabled ==1) { ?>
                var chart2 = new google.visualization.LineChart(document.getElementById('chart2_div'));
                if (OutsideWaterHeaterData != "ERROR"){
                    chart2.draw(OutsideWaterHeaterData, WaterOptions)
                }else if (OutsideWaterData != "ERROR"){
                    chart2.draw(OutsideWaterData, WaterOptions)
                }
                <?php } ?>
			
				//var materialChart = new google.charts.Line(chart_div);
				//materialChart.draw(joinData, google.charts.Line.convertOptions(options));
			
		//	 var rangeFilter = new google.visualization.ControlWrapper({
		//		controlType: 'ChartRangeFilter',
		//		containerId: 'range_filter_div',
		//		options: {
		//			filterColumnIndex: 0,
		//			ui: {
		//				chartOptions: {
		//					height: 50,
		//					width: 600,
		//					chartArea: {
		//						width: '75%'
		//					}
		//				},
		//				minRangeSize: 86400000, // 86400000ms = 1 day
		//				snapToData: true
		//			},
		//		legend: {position: 'Bottom', textStyle: {color: 'blue', fontSize: 16}}
		//		},
		//		view: {
		//			columns: [0, 1, 2]
		//		},
		//		state: {
		//			range: {
		//				// set the starting range to January 2012
		//				start: new Date(2017, 5, 1),
		//				end: new Date(2017, 5, 3)
		//			}
		//		}
		//	});
		//	
		//	 var chart = new google.visualization.ChartWrapper({
		//		chartType: 'LineChart',
		//		containerId: 'chart_div',
		//		options: {
		//			// width and chartArea.width should be the same for the filter and chart
		//			height: 900,
		//			width: 800,
		//			chartArea: {
		//				width: '75%'
		//			}
		//		}
		//	});
		//
			// Create the dashboard
			//var dash = new google.visualization.Dashboard(document.getElementById('dashboard'));
			// bind the chart to the filter
			//dash.bind([rangeFilter], [chart]);
			// draw the dashboard
			//dash.draw(data);
			}

		</script>
		

	</head>
	<body class="normal">
		<?php include_once "Nav.html"; ?>
		<div class="w3-row-padding">
			<form action="/TempChart.php" method="post">
				Days (max 180):
				<input type="number" name="date" min="0" max="180" value="<?php echo $numdays ?>">
				  <input type="submit">
			</form>
		</div>
		<div id="dashboard">
			<div id="chart_div" style="width: 95vw; height: 90vh; margin: 0 auto; "></div>
			<div id="chart2_div" style="width: 95vw; height: 90vh; margin: 0 auto; "></div>
			<div id="range_filter_div"></div>
		</div>
	</body>
</html>
<?php
    include_once $path . "/Footer.php";
?>

<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache'); // recommended to prevent caching of event data.

/**
 * Constructs the SSE data format and flushes that data to the client.
 *
 * @param string $id Timestamp/id of this connection.
 * @param string $msg Line of text that should be transmitted.
 */
function sendMsg($id, $msg) {
  echo "id: $id" . PHP_EOL;
  echo "data: $msg" . PHP_EOL;
  echo PHP_EOL;
  ob_flush();
  flush();
}

$serverTime = time();

$creds = parse_ini_file("/home/pi/secure.cfg",true);
    
    //print_r($creds["database"]["dbusername"]);

    // Create connection
    $conn = new mysqli($creds["database"]["host"], $creds["database"]["dbusername"], $creds["database"]["dbpassword"], $creds["database"]["dbname"]);

    // Check connection
    if($conn->connect_errno > 0){
        die('Unable to connect to database [' . $conn->connect_error . ']');
    }

    $query = "SELECT T.t_Temp FROM TEMPS T RIGHT JOIN DEVICES D ON T.t_Device = D.d_Device WHERE D.d_Role = 'WaterHeater'";
    if(!$result = $conn->query($query)){
	die(print_r('There was an error running the query [' . $conn->error . ']'));
    }
    $Heater = $result->fetch_array()
    
sendMsg($serverTime, $Heater);



mysqli_close($conn);

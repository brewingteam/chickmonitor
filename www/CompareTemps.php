<?php
$numdays = 1;
if( isset($_POST['date']) )
{
	$numdays = $_POST["date"];
}


?> 
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="style.css">

		<!--Load the AJAX API-->
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript">

			// Load the Visualization API and the corechart package.
			//google.charts.load('current', {'packages':['corechart']});
			//google.charts.load('visualization', '1', {packages: ['controls']});
			google.charts.load('current', {'packages':['line', 'corechart']});
			
			// Set a callback to run when the Google Visualization API is loaded.
			google.charts.setOnLoadCallback(drawChart);

			
			function drawChart() {
				var jsonData = $.ajax({
					type: "POST",
					url: "TempData.php",
					data:{date:"<?php echo $numdays ?>",device:"CoopTemperature"},
					dataType: "json",
					async: false
				}).responseText;
				
				var jsonData2 = $.ajax({
					type: "POST",
					url: "TempData.php",
					data:{date:"<?php echo $numdays ?>",device:"CoopTemp"},
					dataType: "json",
					async: false
				}).responseText;
				
				
				
				// Create our data table out of JSON data loaded from server.
				var data1 = new google.visualization.DataTable(jsonData);
				var data2 = new google.visualization.DataTable(jsonData2);
				
				var joinData = google.visualization.data.join(data1, data2, 'full', [[0, 0]], [1], [1]);
				
				var options = {
					title: 'Temperature',
					curveType: 'function',
					legend: { position: 'bottom' },
					//interpolateNulls: true,
				};
				
				 var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
			    //var chart2 = new google.visualization.LineChart(document.getElementById('chart2_div'));
				
				chart.draw(joinData, options);
				//chart2.draw(data2, options);
			
				//var materialChart = new google.charts.Line(chart_div);
				//materialChart.draw(joinData, google.charts.Line.convertOptions(options));
			
		//	 var rangeFilter = new google.visualization.ControlWrapper({
		//		controlType: 'ChartRangeFilter',
		//		containerId: 'range_filter_div',
		//		options: {
		//			filterColumnIndex: 0,
		//			ui: {
		//				chartOptions: {
		//					height: 50,
		//					width: 600,
		//					chartArea: {
		//						width: '75%'
		//					}
		//				},
		//				minRangeSize: 86400000, // 86400000ms = 1 day
		//				snapToData: true
		//			},
		//		legend: {position: 'Bottom', textStyle: {color: 'blue', fontSize: 16}}
		//		},
		//		view: {
		//			columns: [0, 1, 2]
		//		},
		//		state: {
		//			range: {
		//				// set the starting range to January 2012
		//				start: new Date(2017, 5, 1),
		//				end: new Date(2017, 5, 3)
		//			}
		//		}
		//	});
		//	
		//	 var chart = new google.visualization.ChartWrapper({
		//		chartType: 'LineChart',
		//		containerId: 'chart_div',
		//		options: {
		//			// width and chartArea.width should be the same for the filter and chart
		//			height: 900,
		//			width: 800,
		//			chartArea: {
		//				width: '75%'
		//			}
		//		}
		//	});
		//
			// Create the dashboard
			//var dash = new google.visualization.Dashboard(document.getElementById('dashboard'));
			// bind the chart to the filter
			//dash.bind([rangeFilter], [chart]);
			// draw the dashboard
			//dash.draw(data);
			}

		</script>
		

	</head>
	<body class="normal">
		<?php include_once "nav.html"; ?>
		<div class="w3-row-padding">
			<form action="/CompareTemps.php" method="post">
				Days (max 30)
				:
				<input type="number" name="date" min="1" max="30" value="<?php echo $numdays ?>">
				  <input type="submit">
			</form>
		</div>
		<div id="dashboard">
			<div id="chart_div" style="width: 95vw; height: 90vh; margin: 0 auto; "></div>
			<div id="chart2_div" style="width: 95vw; height: 90vh; margin: 0 auto; "></div>
			<div id="range_filter_div"></div>
		</div>
	</body>
</html>
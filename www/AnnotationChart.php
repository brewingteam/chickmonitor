<?php
$numdays = 1;
if( isset($_POST['date']) )
{
	$numdays = $_POST["date"];
}


?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="style.css">

		<!--Load the AJAX API-->
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript">

			// Load the Visualization API and the corechart package.
			//google.charts.load('current', {'packages':['corechart']});
			google.charts.load('current', {'packages':['annotationchart']});

			// Set a callback to run when the Google Visualization API is loaded.
			google.charts.setOnLoadCallback(drawChart);

			// Callback that creates and populates a data table,
			// instantiates the pie chart, passes in the data and
			// draws it.
			function drawChart() {
				var jsonData = $.ajax({
					type: "POST",
					url: "AnnotationData.php",
					data:{date:"<?php echo $numdays ?>"},
					dataType: "json",
					async: false
				}).responseText;
				
				// Create our data table out of JSON data loaded from server.
				var data = new google.visualization.DataTable(jsonData);

				var options = {
					allowHtml: true,
					displayAnnotationsFilter: true,
				};
				
				var chart = new google.visualization.AnnotationChart(document.getElementById('curve_chart'));

				function selectHandler() {
					var selectedItem = chart.getSelection()[0];
					if (selectedItem) {
					  document.getElementById('time').value = data.getValue(selectedItem.row,0);
					  //alert('The user selected ' + data.getValue(selectedItem.row,0));
					}
				}

				// Listen for the 'select' event, and call my function selectHandler() when
				// the user selects something on the chart.
				google.visualization.events.addListener(chart, 'select', selectHandler);
				chart.draw(data, options);
			}

		</script>
		

	</head>
	<body class="normal">
		<?php include_once "nav.html"; ?>
		<div class="w3-row-padding">
<!--			<form action="/ChartHistory.php" method="post">
				Days (max 30)
				:
				<input type="number" name="date" min="1" max="30" value="<?php echo $numdays ?>">
				  <input type="submit">
			</form> -->
		</div>
		<div id="curve_chart" style="height:90vh"></div>
		
		<!-- The Modal -->
		<div id="myModal" class="modal">
		  <!-- Modal content -->
		  <div class="modal-content">
			<div class="modal-header">
			  <span class="close">&times;</span>
			  <h2>Add Notation</h2>
			</div>
			<div class="modal-body">
				<form action="AddNotation.php" method="post">
									<table>
										<tr>
											<td>
												name:
											</td>
											<td>
											<select name="device">
											<?php 
												$servername = "localhost";
												$username   = "monitor";
												$password   = "ChickTemps";
												$dbname     = "ChickTemps";

												// Create connection
												$conn = new mysqli($servername, $username, $password, $dbname);

												// Check connection
												if($conn->connect_errno > 0){
													die('Unable to connect to database [' . $conn->connect_error . ']');
												}

												$sql    = "SELECT d_Name, d_Device FROM DEVICES";
													// Perform queries 
												if(!$DeviceNames = $conn->query($sql)){
													die('There was an error running the query [' . $conn->error . ']');
												}
												while($row = $DeviceNames->fetch_assoc()){
													echo "<option value=".$row['d_Device'].">".$row['d_Name']."</option>";
												}
												$DeviceNames->free();
												$conn->close();
											?>
											</select></td>
										</tr>
										<tr>
											<td>
												Time:
											</td>
											<td><input name="time" type="text" id="time" readonly/></td>
										</tr>
										<tr>
											<td>
												Title:
											</td>
											<td><input name="title" type="text"/></td>
										</tr>
										<tr>
											<td>
												Description:
											</td>
											<td><input name="desc" type="text"/></td>
										</tr>
									</table>
									<button type="submit">Add Notation</button>
								</form>
			</div>
			<div class="modal-footer">
			  <h3></h3>
			</div>
		  </div>

		</div>
		<!-- Trigger/Open The Modal -->
		<button id="myBtn">Open Modal</button>
		<script>
			// Get the modal
			var modal = document.getElementById('myModal');

			// Get the button that opens the modal
			var btn = document.getElementById("myBtn");

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("close")[0];

			// When the user clicks the button, open the modal 
			btn.onclick = function() {
				if (document.getElementById('time').value != "")
				{
					modal.style.display = "block";
				}
			}

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() {
				modal.style.display = "none";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) {
				if (event.target == modal) {
					modal.style.display = "none";
				}
			}
		</script>
	</body>
</html>
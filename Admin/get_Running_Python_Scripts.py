def running_python_scripts():
    for p in psutil.process_iter():
        if not p.cmdline: continue
        if not os.path.basename(p.cmdline[0]).startswith('python'):
            continue
        try: cwd = p.getcwd()
        except psutil._error.AccessDenied: continue
        for n, arg in enumerate(p.cmdline[1:]):
            if arg == '--':
                if len(p.cmdline) > n+1 and p.cmdline[n+1] != '-':
                    path = p.cmdline[n+1]
                    yield os.path.normpath(os.path.join(cwd, path))
                break
            if arg in ('-c', '-m', '-'): break
            if arg.startswith('-'): continue
            yield os.path.normpath(os.path.join(cwd, arg))
            break
running_python_scripts()